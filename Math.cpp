/***************************************************************************
                          Math.cpp  -  description
                             -------------------
    copyright            : (C) 2000 by Kalman Andrasi
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include <fstream.h>
#include "Main.h"
#include "Math.h"
#include "Map.h"
#include "Objects.h"

Math::Math(Kursk* app)
{
	//Constructor doing nothing
}

int Math::distance(int source_x,int source_y,int target_x,int target_y)
{
	int result;

	result=sqrt(pow(abs(target_x-source_x),2)+pow(abs(target_y-source_y),2));
	result=result/TILE_WIDTH;

	return result;
}

int Math::obstacle_height(Kursk* app,int source_x,int source_y,int target_x,int target_y)
{
	int X,Y,DeltaX,DeltaY,XChange,YChange;
	int i,Error,Lenght;

	X=source_x/TILE_WIDTH;
	Y=source_y/TILE_WIDTH;

	DeltaX=target_x/TILE_WIDTH-source_x/TILE_WIDTH;
	DeltaY=target_y/TILE_WIDTH-source_y/TILE_WIDTH;

	if (DeltaX<0)
	{
		XChange=-1;
		DeltaX=-DeltaX;
	}
	else
		XChange=1;

	if (DeltaY<0 )
	{
		YChange=-1;
		DeltaY=-DeltaY;
	}
	else
		YChange=1;

	Error=0;
	i=0;

	if (DeltaX<DeltaY)
	{
		Lenght=DeltaY+1;

		while (i<Lenght)
		{
			Y=Y+YChange;
			Error=Error+DeltaX;

			if (Error>DeltaY)
			{
				X=X+XChange;
				Error=Error-DeltaY;
			}
			//*****read the tile height at(X,Y)
			if (app->scenario_map->data_map[Y][X]=='#')
				return (1);	
			i++;
		}
	}
	else
	{
		Lenght=DeltaX+1;

		while (i<Lenght)
		{
			X=X+XChange;
			Error=Error+DeltaY;

			if (Error>DeltaX)
			{
				Y=Y+YChange;
				Error=Error-DeltaX;
			}

			if (app->scenario_map->data_map[Y][X]=='#')
				return (1);	
			i++;
		}
	}
	return (0);
}

void Math::load_data(GameObject* source)
{
	ifstream fin;
	int i;

	if (source->is_german)
	{
		switch (source->object_id)
		{
		case 0:
			fin.open("Dat/gpzivj.txt");
			break;
		case 1:
			fin.open("Dat/hvywpnsqd.txt");
			break;
		case 2:
			fin.open("Dat/gmarder.txt");
			break;
		case 3:
			fin.open("Dat/gmortar80.txt");
			break;
		case 4:
			fin.open("Dat/crew.txt");
			break;
		}

		fin.getline(source->object_type,25);
		fin >> source->size_xy;
		fin >> source->target_visibility;
		fin >> source->shot_maingun;
		fin >> source->shot_coax;
		fin >> source->movement_point;
		fin >> source->morale;
		fin >> source->number_of_soldiers;
		fin >> source->speed;
		fin >> source->combat_strength;
		fin >> source->range;
		//to hit probability numbers and penetration values
		if (source->object_id==0 || source->object_id==2)
		{
			for (i=0;i<9;i++)
			{
				fin >> source->to_hit[i];
				fin >> source->penetration_value[i];
			}
		}
		else if (source->object_id==1 || source->object_id==4)
		{
			fin >> source->penetration_value[0];
			fin >> source->to_hit[0];
		}
		else if (source->object_id==3)
		{
			for (i=0;i<7;i++)
			{
				fin >> source->to_hit[i];
			}
		}
	}
	else
	{
		switch (source->object_id)
		{
		case 0:
			fin.open("Dat/rt34.txt");
			break;
		case 1:
			fin.open("Dat/rt70.txt");
			break;
		case 2:
			fin.open("Dat/rmortar120.txt");
			break;
		case 3:
			fin.open("Dat/rinfantry.txt");
			break;
		case 4:
			fin.open("Dat/rcrew.txt");
			break;
		}
		fin.getline(source->object_type,25);
		fin >> source->size_xy;
		fin >> source->target_visibility;
		fin >> source->shot_maingun;
		fin >> source->shot_coax;
		fin >> source->movement_point;
		fin >> source->morale;
		fin >> source->number_of_soldiers;
		fin >> source->speed;
		fin >> source->combat_strength;
		fin >> source->range;
		//to hit probability numbers and penetration values
		if (source->object_id==0 || source->object_id==1)
		{
			for (i=0;i<9;i++)
			{
				fin >> source->to_hit[i];
				fin >> source->penetration_value[i];
			}
		}
		else if (source->object_id==2)
		{
			for (i=0;i<7;i++)
			{
				fin >> source->to_hit[i];
			}
		}
		else if (source->object_id==3 || source->object_id==4)
		{
			fin >> source->penetration_value[0];
			fin >> source->to_hit[0];
		}
	}
}

void Math::scenario_result(Kursk* app)
{
	int r_tank_damaged=0;
	int g_tank_damaged=0;
	int r_tank_destroyed=0;
	int g_tank_destroyed=0;
	int r_soldier_dead=0;
	int g_soldier_dead=0;
	int r_soldier_wounded=0;
	int g_soldier_wounded=0;
	int g_tank_crew=0;
	int g_soldier_abandoned=0;

	for (std::list<GameObject*>::iterator it=app->objects.begin();it!=app->objects.end();it++)
	{
		if ((*it)->is_german && ((*it)->object_id==0 ||
			(*it)->object_id==2))
		{
			switch ((*it)->object_status)
			{
			case 1:
			case 7:			//damaged armoured vehicles
				g_tank_damaged++;
				break;
			case 4:			//abandoned vehicles
				g_tank_crew=g_tank_crew+(*it)->number_of_soldiers;
				g_tank_damaged++;
				break;
			case 5:			//destroyed armoured vehicles
			case 6:
				g_soldier_dead=g_soldier_dead+(*it)->number_of_soldiers;
				g_tank_destroyed++;
				break;
			}	
		}
		if ((*it)->is_german && (*it)->object_id==1)
		{
			g_soldier_dead=g_soldier_dead+9-(*it)->number_of_soldiers;
		}
		if ((*it)->is_german && (*it)->object_id==4)
		{
			g_soldier_abandoned=g_soldier_abandoned+(*it)->number_of_soldiers;
		}
	}

	g_soldier_wounded=g_tank_crew-g_soldier_abandoned;

	std::cout << g_tank_damaged << "-Vehicles damaged" << std::endl;
	std::cout << g_tank_destroyed << "-Vehicles destroyed" << std::endl;
	std::cout << g_soldier_wounded << "-Wounded" << std::endl;
	std::cout << g_soldier_dead << "-Dead" << std::endl;
}