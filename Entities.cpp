// Entities.cpp: implementation of the Entities class.
//
//////////////////////////////////////////////////////////////////////
#include "Main.h"
#include "Entities.h"

GameObject::GameObject(int _object_id)
{	
	//load an entity depending on the object_id number
	switch (_object_id)
	{
	case 0:
		std::cout << "loading surfaces" << std::endl;
		hull=CL_Surface::load("Graphics/Objects/gpzivj",app->resources);
		turret=CL_Surface::load("Graphics/Objects/gpzivjt",app->resources);

		is_turret=true;
		break;
	case 1:
		break;
	}
}

void GameObject::show(int x,int y,int hull_frame,int turret_frame)
{
	hull->put_screen(x,y,hull_frame);
	if (is_turret)
		turret->put_screen(x,y,turret_frame);
}

void GameObject::update()
{

}