/***************************************************************************
                          Path.h  -  description
                             -------------------
    copyright            : (C) 2000 by Kalman Andrasi
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#if _MSC_VER >= 1000
#pragma once
#endif 

#ifndef _PATH_H_
#define _PATH_H_
#define FC __fastcall

class tileMap;

#define SHIFT 5         // change this to reflect the size.			
						// Ex. 64x64 tile equals 2^6. or a shift of 6
#define TILESIZE 48		// change this also to reflect tile size. 48x48.

class AStar
{
private:
	struct NODE {		// node structure
		long f, h;
		int g, tmpg;
		int x, y;
		int NodeNum;
		NODE *Parent;
		NODE *Child[8]; // a node may have upto 8+(NULL) children.
		NODE *NextNode; // for filing purposes
	};

	NODE *OPEN;			// the node list pointers
	NODE *CLOSED;
	NODE *PATH;			// pointer to the best path

	struct STACK {      // the stack structure
		NODE *NodePtr;
		STACK *NextStackPtr;
	};

	STACK *Stack;
	
	BOOL isPath;   

    int ROWS,			// tilemap data members, need to be initialisize
        COLS,			// with current map's width and height
        TOTAL_TILES;	// to allocate memory for the
    int *TileMap;		// pointer to the A* own tilemap data array

public:
	// Modify only these 3 public member functions to support Your favorite Map
   	AStar(tileMap* pMap);
    ~AStar();
   	void InitAstarTileMap(tileMap* pMap);                

	// Must be called and be true
    // before getting the node entries. It frees the lists,
    // calls ::Findpath() and returns true if path is accessible
    BOOL NewPath(int sx, int sy, int dx, int dy);  

    BOOL ReachedGoal(void); // Call and check this function before using these 3 following
    void PathNextNode(void) { PATH=PATH->Parent; }
    int NodeGetX(void)      { return PATH->x; }
    int NodeGetY(void)      { return PATH->y; }

    // other usefull functions (do not change them they are used by the A* algorithm)
    int TileNum(int x, int y); // returns tilenum
	int FreeTile(int x, int y); // returns 1 = true if we can move on it

private:
    void BoundaryTiles(void); // sets 1's around the map area. To be called before init the A* map
    void FreeNodes(void);// frees lists memory

    // The A* Algorithm
    void FindPath(int sx, int sy, int dx, int dy);
	NODE *ReturnBestNode(void);
	void GenerateSuccessors(NODE *BestNode, int dx, int dy);
   	void GenerateSucc(NODE *BestNode,int x, int y, int dx, int dy);
	NODE *CheckOPEN(int tilenum);
	NODE *CheckCLOSED(int tilenum);
	void Insert(NODE *Successor);
	void PropagateDown(NODE *Old);
	void Push(NODE *Node);
	NODE *Pop(void);
};

#undef FC
#endif