/***************************************************************************
                          Interface.h  -  description
                             -------------------
    copyright            : (C) 2000 by Kalman Andrasi
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#if _MSC_VER >= 1000
#pragma once
#endif 

#ifndef _INTERFACE_H_
#define _INTERFACE_H_
#define FC __fastcall

class Button;

class Interface  
{
protected:
	Kursk* app;
	CL_Surface* scen_interface;
	CL_Surface* logo;

	Button* move_button;
	Button* continue_button;
	Button* cancel_button;
	Button* arty_button;
	Button* begin_button;
	Button* fire_button;
	Button* endturn_button;
	Button* rally_button;
	Button* quit_button;
	Button* waypoint_button;
	Button* group_button;

public:
	Interface(Kursk* _app);

	virtual void update();
	void draw();

	CL_String message1_target_type;
	CL_String message2_target_distance;
	CL_String message3_hit_chance;
	CL_String message4_hit_results;
	CL_String message5_hit_results;
	CL_String message6_hit_results;
};

#undef FC
#endif 
