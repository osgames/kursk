/***************************************************************************
                          Main.h  -  description
                             -------------------
    copyright            : (C) 2000 by Kalman Andrasi
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#if _MSC_VER >= 1000
#pragma once
#endif 

#ifndef _MAIN_H_
#define _MAIN_H_
#define FC __fastcall

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <iostream.h>
#include <ClanLib/core.h>
#include <ClanLib/stl.h>
#include <math.h>
#include "Random.h"

#define TILE_WIDTH		48
#define TILE_HEIGHT		48

class main_menu;
class tileMap;
class scenario_list_entry;
class weapons_list_entry;
class Interface;
class Minimap;
class GameObject;
class GameObject_German;
class GameObject_Russian;
class Smoke;

enum
{
	mainmenu=1,
	selectscenario,
	w_purchase,
	ai_deployment,
	manual_deployment,		//initial deployment phase
	drag_and_drop,			//manual deployment phase
	g_movement,				//movement phase
	g_fire,					//select the fire mode and your target (german)
	r_fire,					//select the fire mode and your target (russian)
	g_do_fire,				//fire your weapons
	r_do_fire,				//same for russians
	r_recovery,				//russian recovery phase
	r_artillery,			//russian artillery phase
	g_artillery,
	r_movement,				//russian movement phase
	g_recovery,				//german recovery phase
	r_fire_phase,
	end_scenario,
	rally,
	continue_game,
};
 
class Kursk : public CL_ClanApplication
{
public:
	CL_ResourceManager* resources;
	CL_MouseCursorProvider* cursor_provider;
	CL_Surface* mainmenu_background;
	CL_Surface* scenario_background;
	CL_Surface* mouse_pointer;  
	CL_Surface* weapons_picture;
	CL_Surface* button_down;
	CL_Surface* damage_control;
	CL_Surface* weapons_list;
	CL_Surface* gpzivj_damage;
	CL_Surface* map_kursk001;
	CL_Surface* map_trees001;
	CL_Surface* map_mask001;
	CL_Surface* r_victory_point;
	CL_Surface* g_victory_point;
	CL_Surface* mortar80_crater;
	CL_Surface* gdeadsoldier;
	CL_Surface* rdeadsoldier;
	CL_Surface* scenario_lost;
	CL_Surface* sandbag;
	//****************************************
	std::list<GameObject*> objects;
	std::list<GameObject_German*> germans;
	std::list<GameObject_Russian*> russians;
	//****************************************
	Interface* scenario_interface;
	Minimap* scenario_minimap;
	main_menu* menu;
	CL_Font* simple_font;
	CL_Font* simple_font_grey;
	CL_Font* mini_font;
	CL_Font* simple_font_yellow;
	CL_Font* damage_font_white;
	CL_Font* damage_font_yellow;
	CL_Font* damage_font_green;
	CL_Font* damage_font_red;
	CL_Font* description_font_yellow;
	CL_String scenario_description;
	std::list<scenario_list_entry*> selected_scenario;
	std::list<weapons_list_entry*> selected_weapons;

	tileMap* scenario_map;

	Smoke* smoke[50];
	Rand random_number;
	bool crater_visible;
	bool r_is_artillery;
	bool g_is_artillery;
	bool russian_turn;
	int game_state;
	int scenario_number;
	int requisition_point;
	int map_x, map_y;
	int selected_weapons_id[100];
	int number_of_weapons;
	int current_id;
	int selected_count;
	int mortarshell_coordinates_x[60];
	int mortarshell_coordinates_y[60];
	int gdeadsoldier_coordinates_x[60];
	int gdeadsoldier_coordinates_y[60];
	int rdeadsoldier_coordinates_x[60];
	int rdeadsoldier_coordinates_y[60];
	int mortarshot_number;
	int gdeadsoldier_number;
	int rdeadsoldier_number;
	int smoke_number;
	int deadbody_frame[60];
	int rdeadbody_frame[60];
	int number_of_russians;
	int number_of_germans;
	int turn;
	int scenario_turn;
	int delay_for_firing;
	int turn_delay;
	int dead_units;

protected:
	void init_resources();
	void deinit();

public:
	void run_scenario();
	bool init_objects();
	void objects_clean_up();

	virtual char* get_title();
	virtual void init_modules();
	virtual void deinit_modules();
	virtual int main(int argc,char** argv);		
};

#undef FC
#endif
