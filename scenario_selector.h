/***************************************************************************
                          Scenario_selector.h  -  description
                             -------------------
    copyright            : (C) 2000 by Kalman Andrasi
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#if _MSC_VER >= 1000
#pragma once
#endif 

#ifndef _SCENARIO_SELECTOR_H_
#define _SCENARIO_SELECTOR_H_
#define FC __fastcall

class scenario_list_entry;

class scenario_selector
{
public:
	scenario_selector(Kursk *_app);
	void load_scenario_list(CL_String list_path);
	void check_input();
	void draw();

protected:
	void show_scenario_list();
	Kursk *app;
	std::list<scenario_list_entry*> scenario;
	CL_Surface* scen_map;
	int list_top;
	int list_bottom;
};

class scenario_list_entry
{
public:
	scenario_list_entry(CL_String);

	CL_String get_name() {return scenario_name;}
   
	bool selected() {return t_selected;}
	void set_selected(bool sel) { t_selected=sel;}
   
protected:
	CL_String scenario_name;
	bool t_selected;
};

#undef FC
#endif
