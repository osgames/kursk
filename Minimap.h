/***************************************************************************
                          Minimap.h  -  description
                             -------------------
    copyright            : (C) 2000 by Kalman Andrasi
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#if _MSC_VER >= 1000
#pragma once
#endif 

#ifndef _MINIMAP_H_
#define _MINIMAP_H_
#define FC __fastcall

class Minimap  
{
protected:
	Kursk* app;
	CL_Surface* sur_minimap;
	CL_Surface* sur_minimap_dep;
	CL_Surface* r_victory_point_mini;
	CL_Surface* g_victory_point_mini;

public:
	Minimap(Kursk* _app);

	void draw();
	void update();
};

#undef FC
#endif 