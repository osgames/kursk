/***************************************************************************
                          Random.cpp  -  description
                             -------------------
    copyright            : (C) 2000 by Kalman Andrasi
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include "Main.h"
#include "Random.h"

// constructor:
Rand::Rand(uint32 seed)
{
	RandomInit(seed);
	SetInterval(0,99);
}

// returns a random number between 0 and 1:
double Rand::Random()
{
	uint32 x;
	// generate next random number
	x=randbuffer[p1]=_lrotl(randbuffer[p2],R1)+_lrotl(randbuffer[p1],R2);
	// rotate list pointers
	if (--p1<0) p1=KK-1;
	if (--p2<0) p2=KK-1;
#ifdef SELF_TEST
	// perform self-test
	if (randbuffer[p1]==randbufcopy[0] &&
		memcmp(randbuffer,randbufcopy+KK-p1,KK*sizeof(uint32))==0)
	{
		// self-test failed
		if ((p2+KK-p1)%KK!=JJ)
		{
			// note: the way of printing error messages depends on system
			// In Windows you may use FatalAppExit
			//std::cout << "Random number generator not initialized" << std::endl;
		}
		else
		{
			//std::cout << "Random number generator returned to initial state" << std::endl;
		}
			exit(1);
	}
#endif
	// fast conversion to float:
	union
	{
		double randp1;
		uint32 randbits[2];
	};
	randbits[0] = x << 20;
	randbits[1] = (x >> 12) | 0x3FF00000;
	return randp1 - 1.0;
}

int Rand::iRandom()
{
	// get integer random number
	int i=iinterval*Random();
	if (i >= iinterval) i=iinterval;
	return imin + i;
}

// set interval for iRandom
void Rand::SetInterval(int min,int max) 
{
	imin=min;
	iinterval=max-min+1;
}

void Rand::RandomInit(uint32 seed) 
{
	// this function initializes the random number generator.
	int i;
	// make sure seed != 0
	if (seed==0) seed--;

	// make random numbers and put them into the buffer
	for (i=0; i<KK; i++) 
	{
		seed ^= seed << 13; seed ^= seed >> 17; seed ^= seed << 5;
		randbuffer[i] = seed;
	}

	// check that the right data formats are used by compiler:
	union 
	{
		double randp1;
		uint32 randbits[2];
	};
	randp1 = 1.5; assert(randbits[1]==0x3FF80000);

	// initialize pointers to circular buffer
	p1 = 0;  p2 = JJ;
#ifdef SELF_TEST
	// store state for self-test
	memcpy (randbufcopy, randbuffer, KK*sizeof(uint32));
	memcpy (randbufcopy+KK, randbuffer, KK*sizeof(uint32));
#endif
	// randomize some more
	for (i=0; i<9; i++) Random();
}