/***************************************************************************
                          Menu.cpp  -  description
                             -------------------
    copyright            : (C) 2000 by Kalman Andrasi
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include "Main.h"
#include "Menu.h"
#include "scenario_selector.h"
#include "weapons_selector.h"
#include "Interface.h"
#include "Minimap.h"
#include "Math.h"

main_menu::main_menu(Kursk* _app)
{
	app=_app;
	sel=0;
	quit=false;
	app->game_state=mainmenu;
	scenariobanner_gotfocus=CL_Surface::load("Graphics/Menu/scenariobanner_gotfocus",app->resources);
	scenariobanner_pressed=CL_Surface::load("Graphics/Menu/scenariobanner_pressed",app->resources);
}

void main_menu::show_options()
{

}

void main_menu::start_campaign()
{

}

void main_menu::start_scenario()
{
	if(app->init_objects())
	{
		//std::cout << "init objects - success" << std::endl;		
		//std::cout << "Scenario running..." << std::endl;

		app->scenario_interface=new Interface(app);
		app->scenario_minimap=new Minimap(app);

		//the top-left corner of the map
		app->map_x=0;
		app->map_y=0;

		while (CL_Keyboard::get_keycode( CL_KEY_ESCAPE) == false 
			&& quit == false )
		{
			app->run_scenario();

			app->mouse_pointer->put_screen(CL_Mouse::get_x(),CL_Mouse::get_y());
	
			CL_Display::flip_display();
			CL_System::keep_alive();									
		}

		//here comes the result screen
		//****************************
		Math::scenario_result(app);

		app->objects_clean_up();
		//std::cout << "cleaning objects succesfull" << std::endl;
	}
}

void main_menu::weapons_purchase()
{
	if (app->scenario_number==0)
	{
		//initialize a few variables
		app->number_of_weapons=0;

		for (int i=0;i<100;i++)
		{
			app->selected_weapons_id[i]=255;
		}

		app->objects.clear();
		app->selected_weapons.clear();
		weapons_selector *w_selector = new weapons_selector(app);
   
		w_selector->load_weapons_list("weapons.lst");
	 
		while(((CL_Mouse::get_x()>=475 && CL_Mouse::get_x()<=496) &&
		(CL_Mouse::get_y()>=186 && CL_Mouse::get_y()<=208) &&
		CL_Mouse::left_pressed()) == false )
		{
			w_selector->draw();
			w_selector->check_input();

			//app->mouse_pointer->put_screen(CL_Mouse::get_x(),CL_Mouse::get_y());

			CL_Display::flip_display();
			CL_System::keep_alive();
		}

		//button pressed animation
		app->button_down->put_screen(473,185);
		CL_Display::flip_display();
		CL_System::sleep(50);

		app->game_state=manual_deployment;
		start_scenario();
	}
	else
		select_scenario();
}

void main_menu::select_scenario()
{
	app->selected_scenario.clear();
	scenario_selector *selector = new scenario_selector(app);
   
	selector->load_scenario_list("scenario.lst");
	selector->draw();
	 
	while(((CL_Mouse::get_x()>=475 && CL_Mouse::get_x()<=496) &&
		(CL_Mouse::get_y()>=78 && CL_Mouse::get_y()<=99) &&
		CL_Mouse::left_pressed() && app->scenario_number==0)==false)
	{
		selector->draw();
		selector->check_input();
		
		//app->mouse_pointer->put_screen(CL_Mouse::get_x(),CL_Mouse::get_y());

		CL_Display::flip_display();
		CL_System::keep_alive();
	}

	//button pressed animation
	app->button_down->put_screen(473,77);
	CL_Display::flip_display();
	CL_System::sleep(50);

	app->scenario_number=0;		//forcing to play the first scenario
	app->game_state=w_purchase;
	weapons_purchase();
}

void main_menu::check_input()
{
	if ((CL_Mouse::get_x()>=398 && CL_Mouse::get_x()<=584) &&
		(CL_Mouse::get_y()>=225 && CL_Mouse::get_y()<=260) &&
		(app->game_state==mainmenu))
	{
		scenariobanner_gotfocus->put_screen(398,225);
	}
	if ((CL_Mouse::get_x()>=398 && CL_Mouse::get_x()<=584) &&
		(CL_Mouse::get_y()>=225 && CL_Mouse::get_y()<=260) &&
		CL_Mouse::left_pressed() && (app->game_state==mainmenu))
	{
		scenariobanner_pressed->put_screen(398,225);
		CL_System::sleep(50);
		CL_Display::flip_display();

		app->game_state=selectscenario;
		select_scenario();
	}
}

void main_menu::show_menu()
{
	while(CL_Keyboard::get_keycode( CL_KEY_ESCAPE) == false 
	 && quit == false )
	{	
		app->mainmenu_background->put_screen(0,0);

		check_input();

		//app->mouse_pointer->put_screen(CL_Mouse::get_x(),CL_Mouse::get_y());

		CL_Display::flip_display();
		CL_System::keep_alive();
	}
}

