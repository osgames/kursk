/***************************************************************************
                          Smoke.h  -  description
                             -------------------
    copyright            : (C) 2000 by Kalman Andrasi
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef _SMOKE_H_
#define _SMOKE_H_

#if _MSC_VER > 1000
#pragma once
#endif 

class Smoke  
{
private:
	CL_Surface* smoke_surface;

public:
	Smoke(Kursk* app);

	virtual void draw(Kursk* app);

	float smoke_x;
	float smoke_y;
	int smoke_frame;
	int smoke_frame_delay;
};

#endif 
