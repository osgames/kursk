/***************************************************************************
                          Projectile.cpp  -  description
                             -------------------
    copyright            : (C) 2000 by Kalman Andrasi
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include <time.h>
#include "Main.h"
#include "Objects.h"
#include "Projectile.h"

projectile::projectile(Kursk* app,GameObject* source,GameObject* target)
{
	projectile_surface=CL_Surface::load("Graphics/Objects/projectile_surface",app->resources);

	if ((source->object_id==1 && source->is_german) || (source->object_id==3 && !source->is_german))
		explosion=CL_Surface::load("Graphics/Objects/explosion2",app->resources);
	else if ((source->object_id==0 || source->object_id==2) && source->is_german && source->shot_maingun==0)
		explosion=CL_Surface::load("Graphics/Objects/explosion2",app->resources);
	else if ((source->object_id==0 || source->object_id==1) && !source->is_german && source->shot_maingun==0)
		explosion=CL_Surface::load("Graphics/Objects/explosion2",app->resources);
	else if ((source->object_id==3 && source->is_german) || (source->object_id==2 && !source->is_german))
		explosion=CL_Surface::load("Graphics/Objects/explosion1",app->resources);
	else
		explosion=CL_Surface::load("Graphics/Objects/explosion1",app->resources);

	is_dead=false;
	counter=0;
	frame_counter=0;

	start_x=source->x+TILE_WIDTH/2;
	start_y=source->y+TILE_HEIGHT/2;

	if (source->is_firing && !source->is_german)								//russians
	{
		dest_x=source->enemy_x[source->enemy_fired_at]+TILE_WIDTH/2;
		dest_y=source->enemy_y[source->enemy_fired_at]+TILE_WIDTH/2;
	}

	if (source->object_id!=3 && source->is_german)								//german tanks and infantry
	{
		dest_x=target->x+TILE_WIDTH/2;
		dest_y=target->y+TILE_WIDTH/2;
	}
	else if (source->object_id==3 && source->is_german)							//german 8cm mortar
	{
		srand((unsigned)time(NULL));
		dest_x=CL_Mouse::get_x()+app->map_x-20+(rand()%40);
		dest_y=CL_Mouse::get_y()+app->map_y-20+(rand()%40);
		app->mortarshot_number++;
		app->mortarshell_coordinates_x[app->mortarshot_number]=dest_x;
		app->mortarshell_coordinates_y[app->mortarshot_number]=dest_y;
	}

	shell_x=start_x;
	shell_y=start_y;

	calculate_trajectory();
}

projectile::~projectile()
{
	delete projectile_surface;
}

void projectile::calculate_trajectory()
{
	int X,Y,DeltaX,DeltaY,XChange,YChange;
	int i,Error,Lenght;

	trajectory_x = new int[5000];
	trajectory_y = new int[5000];

	X=start_x;
	Y=start_y;

	DeltaX=dest_x-start_x;
	DeltaY=dest_y-start_y;

	if (DeltaX<0)
	{
		XChange=-1;
		DeltaX=-DeltaX;
	}
	else
		XChange=1;

	if (DeltaY<0)
	{
		YChange=-1;
		DeltaY=-DeltaY;
	}
	else
		YChange=1;

	Error=0;
	i=0;

	if (DeltaX<DeltaY)
	{
		Lenght=DeltaY+1;

		while (i<Lenght)
		{

			Y=Y+YChange;
			Error=Error+DeltaX;

			if (Error>DeltaY)
			{
				X=X+XChange;
				Error=Error-DeltaY;
			}

			trajectory_x[i]=X;
			trajectory_y[i]=Y;

			i++ ;
		}
	}
	else
	{
		Lenght=DeltaX+1;

		while (i<Lenght)
		{

			X=X+XChange;
			Error=Error+DeltaY;

			if (Error>DeltaX)
			{
				Y=Y+YChange;
				Error=Error-DeltaX;
			}

			trajectory_x[i] = X;
			trajectory_y[i] = Y;

			i ++ ;
		}
	}
}

void projectile::show(Kursk* app)
{
	if (!is_dead)
		projectile_surface->put_screen(shell_x-app->map_x,shell_y-app->map_y);
}

void projectile::update(Kursk* app)
{
	if (shell_x>=dest_x-TILE_WIDTH/2 && shell_x<=dest_x-TILE_WIDTH/2+TILE_WIDTH &&
		shell_y>=dest_y-TILE_HEIGHT/2 && shell_y<=dest_y-TILE_HEIGHT/2+TILE_HEIGHT)
	{
		is_dead=true;

		projectile_frame_delay=CL_System::get_time();

		shell_x=0;
		shell_y=0;
	}
	else
	{
		shell_x=trajectory_x[counter];
		shell_y=trajectory_y[counter];
		counter+=12;
	}
}
