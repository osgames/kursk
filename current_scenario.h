// current_scenario.h: interface for the current_scenario class.
//
//////////////////////////////////////////////////////////////////////
#ifndef _CURRENT_SCENARIO_H_
#define _CURRENT_SCENARIO_H_

#include <ClanLib/core.h>
#include "scenario_selector.h"

class Kursk;

class current_scenario 
{
protected:
	Kursk* app;

private:
	void init_scenario();

public:
	current_scenario(Kursk* _app);

	void deployment();

};

#endif 