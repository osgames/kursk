/***************************************************************************
                          Map.cpp  -  description
                             -------------------
    copyright            :  
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include "Main.h"
#include "map.h"

tileMap::tileMap()
{
	victory_flag1=0;
	victory_flag2=0;
	victory_flag3=0;
	
	for(int y=0;y<(MAP_HEIGHT);y++)
		for(int x=0;x<(MAP_WIDTH);x++)
	{
		tmp_map[y][x] = 0;
		map[y][x] = 0;
		water_map[y][x] = 0;
		drive_map[y][x] = 0;
		data_map[y][x] = 0;
		tmp_data_map[y][x] = 0;
	}

	for(int i = 0;i < 4;i ++) // sets start positions if none defined
	{
		s_pos_x[i] = i;
		s_pos_y[i] = i;
	}
}

void tileMap::load_map(CL_String _filename)
{
	int x,y;

	//std::cout << "loading scenario map..." << std::endl;
	CL_String track_filename = _filename;
	CL_String drive_filename = track_filename;
	drive_filename += ".data";

	CL_String data_filename = _filename;
	data_filename += ".drive";

	CL_InputSourceProvider * input = CL_InputSourceProvider::create_file_provider("");
	CL_InputSource * file = input -> open_source(track_filename);

	for(y = 0; y < MAP_HEIGHT; y ++)
	{
		for(x = 0; x < MAP_WIDTH; x ++)
		{
			tmp_map[y][x] = file -> read_char8();
		}
		file -> read_char8(); 
		file -> read_char8();
	}
	delete file;					 
	delete input;					 

	//std::cout << "map read from file. Starting editing... " << std::endl;

	for(y = 0; y < MAP_HEIGHT; y ++)
	{
		for(x = 0; x < MAP_WIDTH; x ++)
		{
			if(tmp_map[y][x] == '|') // '|' this is not blitted!
			{
				water_map[y][x] = 0;
			}
			else if(tmp_map[y][x] == 'W')
			{
				water_map[y][x] = 1;
			}
		}
	}
	
	//Obstacle map for pathfinding
	CL_InputSourceProvider * input2 = CL_InputSourceProvider::create_file_provider("");
	CL_InputSource * file2 = input2 -> open_source(drive_filename);

	for(y = 0; y < MAP_HEIGHT; y ++)
	{
		for(x = 0; x < MAP_WIDTH; x ++)
		{
			drive_map[y][x] = file2 -> read_char8();
		}
		file2 -> read_char8();
		file2 -> read_char8();
	}
	
	delete file2;
	delete input2;

	//Obstacle map for LOS
	CL_InputSourceProvider * input3 = CL_InputSourceProvider::create_file_provider("");
	CL_InputSource * file3 = input3 -> open_source(data_filename);

	for(y = 0; y < MAP_HEIGHT; y ++)
	{
		for(x = 0; x < MAP_WIDTH; x ++)
		{
			data_map[y][x] = file3 -> read_char8();
		}
		file3 -> read_char8();
		file3 -> read_char8();
	}

	delete file3;
	delete input3;
}
