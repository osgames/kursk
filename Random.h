/***************************************************************************
                          Random.h  -  description
                             -------------------
    copyright            : (C) 2000 by Kalman Andrasi
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#if _MSC_VER >= 1000
#pragma once
#endif

#ifndef _RANDOM_H_
#define _RANDOM_H_
#define FC __fastcall

#include <stdio.h>
#include <conio.h>
#include <math.h>
#include <memory.h>
#include <stdlib.h>
#include <assert.h>

// define if you want self-test:
#define SELF_TEST
// define 32-bits unsigned integer
typedef unsigned long uint32;
// define parameters
const int KK = 17;
const int JJ = 10;
const int R1 = 13;
const int R2 =  9;

class Rand
{
public:
	void RandomInit(uint32 seed);		// initialization   
	void SetInterval(int min, int max); // set interval for iRandom
	int iRandom();                      // get integer random number
	double Random();                    // get floating point random number
	Rand(uint32 seed=-1);				// constructor
protected:
	int p1, p2;                         // indexes into buffer
	int imin, iinterval;                // interval for iRandom
	uint32 randbuffer[KK];              // history buffer
#ifdef SELF_TEST
	uint32 randbufcopy[KK*2];           // used for self-test
#endif
};

#undef FC
#endif 

