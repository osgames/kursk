/***************************************************************************
                          Menu.h  -  description
                             -------------------
    copyright            : (C) 2000 by Kalman Andrasi
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#if _MSC_VER >= 1000
#pragma once
#endif 

#ifndef _MENU_H_
#define _MENU_H_
#define FC __fastcall

class main_menu
{
public:
	main_menu(Kursk* _app);

	CL_Surface* scenariobanner_pressed;
	CL_Surface* scenariobanner_gotfocus;

	void show_menu();
	void show_options();
	void start_campaign();
	void check_input();
	void select_scenario();
	void weapons_purchase();
	void start_scenario();

	bool quit;
 
protected:
   Kursk* app;
   
   int sel;
};

#undef FC
#endif