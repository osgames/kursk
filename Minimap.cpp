/***************************************************************************
                          Minimap.cpp  -  description
                             -------------------
    copyright            : (C) 2000 by Kalman Andrasi
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include "Main.h"
#include "Minimap.h"
#include "Objects.h"

Minimap::Minimap(Kursk* _app)
{
        app=_app;

        //load the minimap depending on the scenario number
        switch (app->scenario_number)
        {
        case 0:
                sur_minimap=CL_Surface::load("Graphics/Interface/sur_minimap",app->resources);
                sur_minimap_dep=CL_Surface::load("Graphics/Interface/sur_minimap_dep",app->resources);
                break;
        case 1:
                break;
        }

		g_victory_point_mini=CL_Surface::load("Graphics/Interface/g_victory_point_mini",app->resources);
		r_victory_point_mini=CL_Surface::load("Graphics/Interface/r_victory_point_mini",app->resources);
}

void Minimap::draw()
{
        if (app->game_state==drag_and_drop)
                sur_minimap_dep->put_screen(499,22);
        else
                sur_minimap->put_screen(499,22);
}

void Minimap::update()
{
        int tempx,tempy;
        int tempX,tempY;

		//blit the victory locations on the minimap
		//german
		if (app->scenario_map->victory_flag1==0)
			r_victory_point_mini->put_screen(499+105,22+7);
		else
			g_victory_point_mini->put_screen(499+105,22+7);
		if (app->scenario_map->victory_flag2==0)
			r_victory_point_mini->put_screen(499+90,22+40);
		else
			g_victory_point_mini->put_screen(499+90,22+40);
		if (app->scenario_map->victory_flag3==0)
			r_victory_point_mini->put_screen(499+111,22+66);
		else
			g_victory_point_mini->put_screen(499+111,22+66);

        //draw the red retangle
        CL_Display::fill_rect(499+((int)app->map_x/16),22+((int)app->map_y/16),
                529+((int)app->map_x/16),49+((int)app->map_y/16),0.3f,0.0f,0.0f,0.2f);

        //draw the dots
        for (std::list<GameObject*>::iterator it=app->objects.begin();it!=app->objects.end();it++)
        {
                tempx=499+(*it)->x/16;
                tempy=22+(*it)->y/16;

                //limit the dots to the minimap
                if (tempx>618)
                        tempx=618;
                if (tempx<499)
                        tempx=499;
                if (tempy>111)
                        tempy=111;
                if (tempy<22)
                        tempy=22;

                if ((*it)->is_visible && !(*it)->is_dead && !(*it)->is_selected)
                        (*it)->minimap_dot->put_screen(tempx,tempy);
                else if ((*it)->is_visible && !(*it)->is_dead && (*it)->is_selected)
                        (*it)->minimap_dot_w->put_screen(tempx,tempy);
        }
        //minimap navigation
        if (app->game_state!=drag_and_drop)
        {
                if (CL_Mouse::get_x()>=499 && CL_Mouse::get_x()<=618 && 
                        CL_Mouse::get_y()>=22 && CL_Mouse::get_y()<=111 && CL_Mouse::left_pressed())
                {
                        //temporary variable to store the mouse pos. on the minimap
                        tempX = (int)(CL_Mouse::get_x()-499)/30;
                        tempY = (int)(CL_Mouse::get_y()-22)/27;

                        switch (tempX)
                        {
                        case 0:
                                app->map_x=0;
                                break;
                        case 1:
                                app->map_x=480;
                                break;
                        case 2:
                                app->map_x=960;
                                break;
                        case 3:
                                app->map_x=1440;
                                break;
                        }

                        switch (tempY)
                        {
                        case 0:
                                app->map_y=0;
                                break;
                        case 1:
                                app->map_y=480;
                                break;
                        case 2:
                                app->map_y=960;
                                break;
                        }
                }
        }
}
