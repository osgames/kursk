/***************************************************************************
                          Miscellaneous.h  -  description
                             -------------------
    copyright            : (C) 2000 by Kalman Andrasi
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#if _MSC_VER >= 1000
#pragma once
#endif 

#ifndef _MISCELLANEOUS_H_
#define _MISCELLANEOUS_H_
#define FC __fastcall

class Analysis
{
public:
	Analysis(Kursk* app);

	static void target_eliminated(GameObject* source,GameObject* target,Kursk* app);
	static void r_target_eliminated(GameObject* source,GameObject* target,Kursk* app);
};

#undef FC
#endif
