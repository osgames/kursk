/***************************************************************************
                          Button.h  -  description
                             -------------------
    copyright            : (C) 2000 by Kalman Andrasi
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#if _MSC_VER >= 1000
#pragma once
#endif 

#ifndef _BUTTON_H_
#define _BUTTON_H_
#define FC __fastcall

/** The Button class, part of the game interface.
    @author Kalman Andrasi <andrasik@un.org>
*/
class Button
{
protected:
	CL_Surface* button_surface;				//button images
	CL_Surface* button_surface_down;
	CL_Surface* button_surface_notavail;

private:

public:
	Button(Kursk* app,int _x,int _y,int type);

	enum					//determines the type of the button
	{
		artillery,
		move,
		fire,
		rally,
		endturn,
		begin,
		cancel,
		cont,
		quit,
		waypoint,
		group,
	};

	int x;					// x ccordinates
	int y;					// y coordinates of the button
	bool available;			// availability

	void draw(int state);
	bool is_pressed();
};

#undef FC
#endif