/***************************************************************************
                          Interface.cpp  -  description
                             -------------------
    copyright            : (C) 2000 by Kalman Andrasi
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include "Main.h"
#include "Interface.h"
#include "minimap.h"
#include "menu.h"
#include "Objects.h"
#include "Button.h"

Interface::Interface(Kursk* _app)
{
	app=_app;

	message1_target_type="";
	message2_target_distance="";
	message3_hit_chance="";
	message4_hit_results="";
	message5_hit_results="";
	message6_hit_results="";

	scen_interface=CL_Surface::load("Graphics/Interface/scen_interface",app->resources);
	
	move_button=new Button(app,540,249,move_button->move);
	continue_button=new Button(app,406,447,continue_button->cont);
	cancel_button=new Button(app,566,443,cancel_button->cancel);
	arty_button=new Button(app,495,249,arty_button->artillery);
	begin_button=new Button(app,499,443,begin_button->begin);
	fire_button=new Button(app,585,249,fire_button->fire);
	endturn_button=new Button(app,495,388,endturn_button->endturn);
	rally_button=new Button(app,495,292,rally_button->rally);
	quit_button=new Button(app,585,388,quit_button->quit);
	waypoint_button=new Button(app,540,292,waypoint_button->waypoint);
	group_button=new Button(app,585,292,group_button->group);
}

void Interface::update()
{

	switch (app->game_state)
	{
		case manual_deployment:
			app->simple_font_yellow->print_left(10,440,"UNIT DEPLOYMENT PHASE");
			app->damage_font_green->print_left(10,452,"Click on a unit and hold the mouse button to drag it.");
			app->damage_font_green->print_left(10,462,"Press Begin when all units are in place.");
			continue_button->draw(1);

			if (continue_button->is_pressed())
			{
				app->game_state=drag_and_drop;
			}

			break;

		case drag_and_drop:

			begin_button->draw(1);
			cancel_button->draw(1);

			//if there is artillery put the arty button
			if (app->g_is_artillery)
				arty_button->draw(1);
			else
				arty_button->draw(0);
			move_button->draw(0);
			fire_button->draw(0);
			rally_button->draw(0);
			endturn_button->draw(0);
			quit_button->draw(0);
			waypoint_button->draw(0);
			group_button->draw(0);

			if (begin_button->is_pressed())
			{
				app->game_state=g_movement;
			}

			if (cancel_button->is_pressed())
			{
				app->game_state=selectscenario;
				app->menu->select_scenario();
			}

			break;

		case g_movement:

			//if there is artillery put the arty button
			if (app->g_is_artillery)
				arty_button->draw(1);
			else
				arty_button->draw(0);
			move_button->draw(0);
			fire_button->draw(1);
			rally_button->draw(1);
			endturn_button->draw(1);
			quit_button->draw(1);
			waypoint_button->draw(1);
			group_button->draw(1);
			
			/*app->damage_font_yellow->print_left(494,122,"Unit:");
			app->damage_font_yellow->print_left(494,134,"Cmndr:");
			app->damage_font_yellow->print_left(494,146,"Status:");

			app->damage_font_yellow->print_left(494,166,"Enemy seen:");
			app->damage_font_yellow->print_left(494,178,"Shots/Turn:");
			app->damage_font_yellow->print_left(494,190,"Move/Turn:");

			app->damage_font_yellow->print_left(494,210,"Supression:");
			app->damage_font_yellow->print_left(494,222,"Experience:");*/

			logo=CL_Surface::load("Graphics/Interface/hvastica",app->resources);
			logo->put_screen(494,435);
			app->simple_font_yellow->print_left(515,440,"Movement phase");
			app->simple_font->print_left(515,456,CL_String("Turn: ")+CL_String(app->turn) + CL_String(" of ")+CL_String(app->scenario_turn));

			{for (std::list<GameObject*>::iterator it=app->objects.begin();it!=app->objects.end();it++)
			
				if ((*it)->is_selected)
				{
					app->damage_font_white->print_left(538,121,(*it)->object_type);
					app->damage_font_white->print_left(538,133,(*it)->commander);

					switch ((*it)->object_status)
					{
					case 0:
						app->damage_font_green->print_left(538,145,"READY");
						break;
					case 1:		//gun malfunction
						app->damage_font_yellow->print_left(538,145,"GUN MALFUNCTION");
						break;
					case 2:
						app->damage_font_yellow->print_left(538,145,"PINNED");
						break;
					case 3:
						app->damage_font_red->print_left(538,145,"WITHDRAWING");
						break;
					case 4:
						app->damage_font_yellow->print_left(538,145,"ABANDONED");
						break;
					case 5:
						app->damage_font_red->print_left(538,145,"WRECK");
						break;
					case 6:
						app->damage_font_red->print_left(538,145,"BURNING");
						break;
					case 7:
						app->damage_font_yellow->print_left(538,145,"IMMOBILIZED");
						break;
					case 8:
						app->damage_font_red->print_left(538,145,"ROUTED");
						break;
					}
					if (!(*it)->is_dead)
					{
						app->damage_font_white->print_left(567,165,CL_String((*it)->enemy_seen(app)));
						app->damage_font_white->print_left(567,177,CL_String((*it)->shot_maingun));
						app->damage_font_white->print_left(572,177,"/");
						app->damage_font_white->print_left(577,177,CL_String((*it)->shot_coax));
						app->damage_font_white->print_left(567,189,CL_String((*it)->movement_point/48));
						app->damage_font_white->print_left(567,209,CL_String((*it)->suppressed));
					}

					break;
				}
			}
			//check if the fire button was pressed
			if (fire_button->is_pressed())
			{
				CL_MouseCursor::hide();
				app->mouse_pointer=CL_Surface::load("Graphics/Menu/fire_cursor",app->resources);

				app->scenario_interface->message4_hit_results="";
				app->scenario_interface->message5_hit_results="";
				app->scenario_interface->message6_hit_results="";

				app->game_state=g_fire;
			}
			//check if the "end turn" button was pressed
			if (endturn_button->is_pressed())
			{
				app->russian_turn=true;

				for (std::list<GameObject*>::iterator it=app->objects.begin();it!=app->objects.end();it++)
				{
					if (!(*it)->is_german && (*it)->object_status==0)
					{
						switch ((*it)->object_id)
						{
							case 0:			//T-34
								(*it)->shot_maingun=1+rand()%2;
								(*it)->shot_coax=rand()%3;
								break;
							case 1:			//T-70
								(*it)->shot_maingun=1+rand()%3;
								(*it)->shot_coax=rand()%3;
								break;
							case 2:			//120mm Mortar
								(*it)->shot_maingun=1+rand()%3;
								(*it)->shot_coax=0;
								break;
							case 3:			//Infantry
								(*it)->shot_maingun=0;
								(*it)->shot_coax=1+rand()%2;
								break;
						}
					}
				}
				app->turn_delay=CL_System::get_time();
				//app->game_state=r_recovery;
				app->game_state=r_fire_phase;
			}
			//check if the quit button was pressed
			if (quit_button->is_pressed())
			{
				app->menu->quit=true;
			}
			//check if rally button was down
			if (rally_button->is_pressed())
			{
				app->game_state=rally;
			}

			break;

		case rally:

			arty_button->draw(0);
			move_button->draw(0);
			fire_button->draw(0);
			rally_button->draw(0);
			endturn_button->draw(0);
			quit_button->draw(0);
			waypoint_button->draw(0);
			group_button->draw(0);

			logo=CL_Surface::load("Graphics/Interface/hvastica",app->resources);
			logo->put_screen(494,435);
			app->simple_font_yellow->print_left(515,440,"Rally");
			app->simple_font->print_left(515,456,CL_String("Turn: ")+CL_String(app->turn) + CL_String(" of ")+CL_String(app->scenario_turn));

			{for (std::list<GameObject*>::iterator it=app->objects.begin();it!=app->objects.end();it++)
			
				if ((*it)->is_selected)
				{
					app->damage_font_white->print_left(538,121,(*it)->object_type);
					app->damage_font_white->print_left(538,133,(*it)->commander);

					switch ((*it)->object_status)
					{
					case 0:
						app->damage_font_green->print_left(538,145,"READY");
						break;
					case 1:		//gun malfunction
						app->damage_font_yellow->print_left(538,145,"GUN MALFUNCTION");
						break;
					case 2:
						app->damage_font_yellow->print_left(538,145,"PINNED");
						break;
					case 3:
						app->damage_font_red->print_left(538,145,"WITHDRAWING");
						break;
					case 4:
						app->damage_font_yellow->print_left(538,145,"ABANDONED");
						break;
					case 5:
						app->damage_font_red->print_left(538,145,"WRECK");
						break;
					case 6:
						app->damage_font_red->print_left(538,145,"BURNING");
						break;
					case 7:
						app->damage_font_yellow->print_left(538,145,"IMMOBILIZED");
						break;
					case 8:
						app->damage_font_red->print_left(538,145,"ROUTED");
						break;
					}
					if (!(*it)->is_dead)
					{
						app->damage_font_white->print_left(567,165,CL_String((*it)->enemy_seen(app)));
						app->damage_font_white->print_left(567,177,CL_String((*it)->shot_maingun));
						app->damage_font_white->print_left(572,177,"/");
						app->damage_font_white->print_left(577,177,CL_String((*it)->shot_coax));
						app->damage_font_white->print_left(567,189,CL_String((*it)->movement_point/48));
						app->damage_font_white->print_left(567,209,CL_String((*it)->suppressed));
					}
				}
			}

			app->damage_font_white->print_left(60,440,app->scenario_interface->message1_target_type);

			continue_button->draw(1);

			break;

		case continue_game:

			arty_button->draw(0);
			move_button->draw(0);
			fire_button->draw(0);
			rally_button->draw(0);
			endturn_button->draw(0);
			quit_button->draw(0);
			waypoint_button->draw(0);
			group_button->draw(0);

			app->damage_font_white->print_left(60,440,app->scenario_interface->message1_target_type);

			continue_button->draw(1);

			if (continue_button->is_pressed())
			{
				app->game_state=g_movement;
			}

			break;

		case g_fire:
			//if there is artillery put the arty button
			if (app->g_is_artillery)
				arty_button->draw(1);
			else
				arty_button->draw(0);
			move_button->draw(1);
			fire_button->draw(0);
			rally_button->draw(0);
			endturn_button->draw(1);
			quit_button->draw(1);
			waypoint_button->draw(1);
			group_button->draw(1);

			app->damage_font_yellow->print_left(10,438,"Target:");
			app->damage_font_yellow->print_left(10,450,"Distance:");
			app->damage_font_yellow->print_left(10,462,"Hit Chance:");
			//messaging
			app->damage_font_white->print_left(60,440,message1_target_type);
			app->damage_font_white->print_right(85,452,message2_target_distance);
			app->damage_font_white->print_right(100,452,"m");
			app->damage_font_white->print_right(85,462,message3_hit_chance);
			app->damage_font_white->print_right(100,462,"%");

			app->damage_font_yellow->print_left(230,438,"Hit:");
			app->damage_font_yellow->print_left(230,450,"Damage:");
			app->damage_font_yellow->print_left(230,462,"Morale:");

			app->damage_font_white->print_left(300,440,message4_hit_results);
			app->damage_font_white->print_left(300,452,message5_hit_results);
			app->damage_font_white->print_left(300,462,message6_hit_results);
			
			/*app->damage_font_yellow->print_left(494,122,"Unit:");
			app->damage_font_yellow->print_left(494,134,"Cmndr:");
			app->damage_font_yellow->print_left(494,146,"Status:");

			app->damage_font_yellow->print_left(494,166,"Enemy seen:");
			app->damage_font_yellow->print_left(494,178,"Shots/Turn:");
			app->damage_font_yellow->print_left(494,190,"Move/Turn:");

			app->damage_font_yellow->print_left(494,210,"Supression:");
			app->damage_font_yellow->print_left(494,222,"Experience:");*/

			logo=CL_Surface::load("Graphics/Interface/hvastica",app->resources);
			logo->put_screen(494,435);
			app->simple_font_yellow->print_left(515,440,"Fire phase");
			app->simple_font->print_left(515,456,CL_String("Turn: ")+CL_String(app->turn) + CL_String(" of ")+CL_String(app->scenario_turn));

			{for (std::list<GameObject*>::iterator it=app->objects.begin();it!=app->objects.end();it++)
			
				if ((*it)->is_selected)
				{
					app->damage_font_white->print_left(538,121,(*it)->object_type);
					app->damage_font_white->print_left(538,133,(*it)->commander);

					switch ((*it)->object_status)
					{
					case 0:
						app->damage_font_green->print_left(538,145,"READY");
						break;
					case 1:		//gun malfunction
						app->damage_font_yellow->print_left(538,145,"GUN MALFUNCTION");
						break;
					case 2:
						app->damage_font_yellow->print_left(538,145,"PINNED");
						break;
					case 3:
						app->damage_font_red->print_left(538,145,"WITHDRAWING");
						break;
					case 4:
						app->damage_font_yellow->print_left(538,145,"ABANDONED");
						break;
					case 5:
						app->damage_font_red->print_left(538,145,"WRECK");
						break;
					case 6:
						app->damage_font_red->print_left(538,145,"BURNING");
						break;
					case 7:
						app->damage_font_yellow->print_left(538,145,"IMMOBILIZED");
						break;
					case 8:
						app->damage_font_red->print_left(538,145,"ROUTED");
						break;
					}
					if (!(*it)->is_dead)
					{
						app->damage_font_white->print_left(567,165,CL_String((*it)->enemy_seen(app)));
						app->damage_font_white->print_left(567,177,CL_String((*it)->shot_maingun));
						app->damage_font_white->print_left(572,177,"/");
						app->damage_font_white->print_left(577,177,CL_String((*it)->shot_coax));
						app->damage_font_white->print_left(567,189,CL_String((*it)->movement_point/48));
						app->damage_font_white->print_left(567,209,CL_String((*it)->suppressed));
					}

					break;
				}
			}

			//check if the move button was pressed
			if (move_button->is_pressed())
			{
				CL_MouseCursor::show();
				app->mouse_pointer=CL_Surface::load("Graphics/Menu/mouse_pointer",app->resources);

				app->game_state=g_movement;
			}
			//check if the "end turn" button was pressed
			if (endturn_button->is_pressed())
			{
				CL_MouseCursor::show();
				app->mouse_pointer=CL_Surface::load("Graphics/Menu/mouse_pointer",app->resources);

				app->russian_turn=true;

				for (std::list<GameObject*>::iterator it=app->objects.begin();it!=app->objects.end();it++)
				{
					if (!(*it)->is_german && (*it)->object_status==0)
					{
						switch ((*it)->object_id)
						{
							case 0:			//T-34
								(*it)->shot_maingun=1+rand()%2;
								(*it)->shot_coax=rand()%3;
								break;
							case 1:			//T-70
								(*it)->shot_maingun=1+rand()%3;
								(*it)->shot_coax=rand()%3;
								break;
							case 2:			//120mm Mortar
								(*it)->shot_maingun=1+rand()%3;
								(*it)->shot_coax=0;
								break;
							case 3:			//Infantry
								(*it)->shot_maingun=0;
								(*it)->shot_coax=1+rand()%2;
								break;
						}
					}
				}

				app->turn_delay=CL_System::get_time();
				//app->game_state=r_recovery;
				app->game_state=r_fire_phase;
			}
			//check if the quit button was pressed
			if (quit_button->is_pressed())
			{
				app->menu->quit=true;
			}
			//check if rally button was down
			//if (rally_button->is_pressed())
			//{
			//	app->game_state=rally;
			//}

			break;

		case g_do_fire:
			app->damage_font_yellow->print_left(10,462,"Hit Chance:");
			app->damage_font_white->print_right(85,462,message3_hit_chance);
			app->damage_font_white->print_right(100,462,"%");

			if (app->g_is_artillery)
				arty_button->draw(1);
			else
				arty_button->draw(0);
			move_button->draw(0);
			fire_button->draw(0);
			rally_button->draw(0);
			endturn_button->draw(0);
			quit_button->draw(0);
			waypoint_button->draw(0);
			group_button->draw(0);

			app->damage_font_yellow->print_left(10,438,"Target:");
			app->damage_font_yellow->print_left(10,450,"Distance:");
			app->damage_font_yellow->print_left(10,462,"Hit Chance:");
			//messaging
			app->damage_font_white->print_left(60,440,message1_target_type);
			app->damage_font_white->print_right(85,452,message2_target_distance);
			app->damage_font_white->print_right(100,452,"m");
			app->damage_font_white->print_right(85,462,message3_hit_chance);
			app->damage_font_white->print_right(100,462,"%");

			app->damage_font_yellow->print_left(230,438,"Hit:");
			app->damage_font_yellow->print_left(230,450,"Damage:");
			app->damage_font_yellow->print_left(230,462,"Morale:");

			app->damage_font_white->print_left(300,440,message4_hit_results);
			app->damage_font_white->print_left(300,452,message5_hit_results);
			app->damage_font_white->print_left(300,462,message6_hit_results);
			
			/*app->damage_font_yellow->print_left(494,122,"Unit:");
			app->damage_font_yellow->print_left(494,134,"Cmndr:");
			app->damage_font_yellow->print_left(494,146,"Status:");

			app->damage_font_yellow->print_left(494,166,"Enemy seen:");
			app->damage_font_yellow->print_left(494,178,"Shots/Turn:");
			app->damage_font_yellow->print_left(494,190,"Move/Turn:");

			app->damage_font_yellow->print_left(494,210,"Supression:");
			app->damage_font_yellow->print_left(494,222,"Experience:");*/

			logo=CL_Surface::load("Graphics/Interface/hvastica",app->resources);
			logo->put_screen(494,435);
			app->simple_font_yellow->print_left(515,440,"Fire phase");
			app->simple_font->print_left(515,456,CL_String("Turn: ")+CL_String(app->turn) + CL_String(" of ")+CL_String(app->scenario_turn));

			{for (std::list<GameObject*>::iterator it=app->objects.begin();it!=app->objects.end();it++)
			
				if ((*it)->is_selected)
				{
					app->damage_font_white->print_left(538,121,(*it)->object_type);
					app->damage_font_white->print_left(538,133,(*it)->commander);

					switch ((*it)->object_status)
					{
					case 0:
						app->damage_font_green->print_left(538,145,"READY");
						break;
					case 1:		//gun malfunction
						app->damage_font_yellow->print_left(538,145,"GUN MALFUNCTION");
						break;
					case 2:
						app->damage_font_yellow->print_left(538,145,"PINNED");
						break;
					case 3:
						app->damage_font_red->print_left(538,145,"WITHDRAWING");
						break;
					case 4:
						app->damage_font_yellow->print_left(538,145,"ABANDONED");
						break;
					case 5:
						app->damage_font_red->print_left(538,145,"WRECK");
						break;
					case 6:
						app->damage_font_red->print_left(538,145,"BURNING");
						break;
					case 7:
						app->damage_font_yellow->print_left(538,145,"IMMOBILIZED");
						break;
					case 8:
						app->damage_font_red->print_left(538,145,"ROUTED");
						break;
					}
					if (!(*it)->is_dead)
					{
						app->damage_font_white->print_left(567,165,CL_String((*it)->enemy_seen(app)));
						app->damage_font_white->print_left(567,177,CL_String((*it)->shot_maingun));
						app->damage_font_white->print_left(572,177,"/");
						app->damage_font_white->print_left(577,177,CL_String((*it)->shot_coax));
						app->damage_font_white->print_left(567,189,CL_String((*it)->movement_point/48));
						app->damage_font_white->print_left(567,209,CL_String((*it)->suppressed));
					}

					break;
				}
			}

			break;

		case r_fire:
			//if there is artillery put the arty button
			if (app->g_is_artillery)
				arty_button->draw(1);
			else
				arty_button->draw(0);
			move_button->draw(0);
			fire_button->draw(0);
			rally_button->draw(0);
			endturn_button->draw(0);
			quit_button->draw(0);
			waypoint_button->draw(0);
			group_button->draw(0);

			app->damage_font_yellow->print_left(10,438,"Target:");
			app->damage_font_yellow->print_left(10,450,"Distance:");
			app->damage_font_yellow->print_left(10,462,"Hit Chance:");
			//messaging
			app->damage_font_white->print_left(60,440,message1_target_type);
			app->damage_font_white->print_right(85,452,message2_target_distance);
			app->damage_font_white->print_right(100,452,"m");
			app->damage_font_white->print_right(85,462,message3_hit_chance);
			app->damage_font_white->print_right(100,462,"%");

			app->damage_font_yellow->print_left(230,438,"Hit:");
			app->damage_font_yellow->print_left(230,450,"Damage:");
			app->damage_font_yellow->print_left(230,462,"Morale:");

			app->damage_font_white->print_left(300,440,message4_hit_results);
			app->damage_font_white->print_left(300,452,message5_hit_results);
			app->damage_font_white->print_left(300,462,message6_hit_results);
			
			/*app->damage_font_yellow->print_left(494,122,"Unit:");
			app->damage_font_yellow->print_left(494,134,"Cmndr:");
			app->damage_font_yellow->print_left(494,146,"Status:");

			app->damage_font_yellow->print_left(494,166,"Enemy seen:");
			app->damage_font_yellow->print_left(494,178,"Shots/Turn:");
			app->damage_font_yellow->print_left(494,190,"Move/Turn:");

			app->damage_font_yellow->print_left(494,210,"Supression:");
			app->damage_font_yellow->print_left(494,222,"Experience:");*/

			logo=CL_Surface::load("Graphics/Interface/star",app->resources);
			logo->put_screen(494,435);
			app->simple_font_yellow->print_left(515,440,"Fire phase");
			app->simple_font->print_left(515,456,CL_String("Turn: ")+CL_String(app->turn) + CL_String(" of ")+CL_String(app->scenario_turn));

			{for (std::list<GameObject*>::iterator it=app->objects.begin();it!=app->objects.end();it++)
			
				if ((*it)->is_selected)
				{
					app->damage_font_white->print_left(538,121,(*it)->object_type);
					app->damage_font_white->print_left(538,133,(*it)->commander);

					switch ((*it)->object_status)
					{
					case 0:
						app->damage_font_green->print_left(538,145,"READY");
						break;
					case 1:		//gun malfunction
						app->damage_font_yellow->print_left(538,145,"GUN MALFUNCTION");
						break;
					case 2:
						app->damage_font_yellow->print_left(538,145,"PINNED");
						break;
					case 3:
						app->damage_font_red->print_left(538,145,"WITHDRAWING");
						break;
					case 4:
						app->damage_font_yellow->print_left(538,145,"ABANDONED");
						break;
					case 5:
						app->damage_font_red->print_left(538,145,"WRECK");
						break;
					case 6:
						app->damage_font_red->print_left(538,145,"BURNING");
						break;
					case 7:
						app->damage_font_yellow->print_left(538,145,"IMMOBILIZED");
						break;
					case 8:
						app->damage_font_red->print_left(538,145,"ROUTED");
						break;
					}
					if (!(*it)->is_dead)
					{
						app->damage_font_white->print_left(567,165,CL_String((*it)->enemy_seen(app)));
						app->damage_font_white->print_left(567,177,CL_String((*it)->shot_maingun));
						app->damage_font_white->print_left(572,177,"/");
						app->damage_font_white->print_left(577,177,CL_String((*it)->shot_coax));
						app->damage_font_white->print_left(567,189,CL_String((*it)->movement_point/48));
						app->damage_font_white->print_left(567,209,CL_String((*it)->suppressed));
					}

					break;
				}
			}

			break;
		case r_do_fire:
			//if there is artillery put the arty button
			if (app->g_is_artillery)
				arty_button->draw(1);
			else
				arty_button->draw(0);
			move_button->draw(0);
			fire_button->draw(0);
			rally_button->draw(0);
			endturn_button->draw(0);
			quit_button->draw(0);
			waypoint_button->draw(0);
			group_button->draw(0);

			app->damage_font_yellow->print_left(10,438,"Target:");
			app->damage_font_yellow->print_left(10,450,"Distance:");
			app->damage_font_yellow->print_left(10,462,"Hit Chance:");
			//messaging
			app->damage_font_white->print_left(60,440,message1_target_type);
			app->damage_font_white->print_right(85,452,message2_target_distance);
			app->damage_font_white->print_right(100,452,"m");
			app->damage_font_white->print_right(85,462,message3_hit_chance);
			app->damage_font_white->print_right(100,462,"%");

			app->damage_font_yellow->print_left(230,438,"Hit:");
			app->damage_font_yellow->print_left(230,450,"Damage:");
			app->damage_font_yellow->print_left(230,462,"Morale:");

			app->damage_font_white->print_left(300,440,message4_hit_results);
			app->damage_font_white->print_left(300,452,message5_hit_results);
			app->damage_font_white->print_left(300,462,message6_hit_results);
			
			/*app->damage_font_yellow->print_left(494,122,"Unit:");
			app->damage_font_yellow->print_left(494,134,"Cmndr:");
			app->damage_font_yellow->print_left(494,146,"Status:");

			app->damage_font_yellow->print_left(494,166,"Enemy seen:");
			app->damage_font_yellow->print_left(494,178,"Shots/Turn:");
			app->damage_font_yellow->print_left(494,190,"Move/Turn:");

			app->damage_font_yellow->print_left(494,210,"Supression:");
			app->damage_font_yellow->print_left(494,222,"Experience:");*/

			logo=CL_Surface::load("Graphics/Interface/star",app->resources);
			logo->put_screen(494,435);
			app->simple_font_yellow->print_left(515,440,"Fire phase");
			app->simple_font->print_left(515,456,CL_String("Turn: ")+CL_String(app->turn) + CL_String(" of ")+CL_String(app->scenario_turn));

			{for (std::list<GameObject*>::iterator it=app->objects.begin();it!=app->objects.end();it++)
			
				if ((*it)->is_selected)
				{
					app->damage_font_white->print_left(538,121,(*it)->object_type);
					app->damage_font_white->print_left(538,133,(*it)->commander);

					switch ((*it)->object_status)
					{
					case 0:
						app->damage_font_green->print_left(538,145,"READY");
						break;
					case 1:		//gun malfunction
						app->damage_font_yellow->print_left(538,145,"GUN MALFUNCTION");
						break;
					case 2:
						app->damage_font_yellow->print_left(538,145,"PINNED");
						break;
					case 3:
						app->damage_font_red->print_left(538,145,"WITHDRAWING");
						break;
					case 4:
						app->damage_font_yellow->print_left(538,145,"ABANDONED");
						break;
					case 5:
						app->damage_font_red->print_left(538,145,"WRECK");
						break;
					case 6:
						app->damage_font_red->print_left(538,145,"BURNING");
						break;
					case 7:
						app->damage_font_yellow->print_left(538,145,"IMMOBILIZED");
						break;
					case 8:
						app->damage_font_red->print_left(538,145,"ROUTED");
						break;
					}
					if (!(*it)->is_dead)
					{
						app->damage_font_white->print_left(567,165,CL_String((*it)->enemy_seen(app)));
						app->damage_font_white->print_left(567,177,CL_String((*it)->shot_maingun));
						app->damage_font_white->print_left(572,177,"/");
						app->damage_font_white->print_left(577,177,CL_String((*it)->shot_coax));
						app->damage_font_white->print_left(567,189,CL_String((*it)->movement_point/48));
						app->damage_font_white->print_left(567,209,CL_String((*it)->suppressed));
					}

					break;
				}
			}

			break;

		case r_fire_phase:

			logo=CL_Surface::load("Graphics/Interface/star",app->resources);
			logo->put_screen(494,435);
			app->simple_font_yellow->print_left(515,440,"Fire phase");
			app->simple_font->print_left(515,456,CL_String("Turn: ")+CL_String(app->turn) + CL_String(" of ")+CL_String(app->scenario_turn));

			break;

		case r_recovery:

			logo=CL_Surface::load("Graphics/Interface/star",app->resources);
			logo->put_screen(494,435);
			app->simple_font_yellow->print_left(515,440,"Recovery phase");
			app->simple_font->print_left(515,456,CL_String("Turn: ")+CL_String(app->turn) + CL_String(" of ")+CL_String(app->scenario_turn));

			break;

		case r_artillery:

			logo=CL_Surface::load("Graphics/Interface/star",app->resources);
			logo->put_screen(494,435);
			app->simple_font_yellow->print_left(515,440,"Artillery strike");
			app->simple_font->print_left(515,456,CL_String("Turn: ")+CL_String(app->turn) + CL_String(" of ")+CL_String(app->scenario_turn));

			break;

		case r_movement:

			logo=CL_Surface::load("Graphics/Interface/star",app->resources);
			logo->put_screen(494,435);
			app->simple_font_yellow->print_left(515,440,"Movement phase");
			app->simple_font->print_left(515,456,CL_String("Turn: ")+CL_String(app->turn) + CL_String(" of ")+CL_String(app->scenario_turn));

			if (CL_System::get_time()>app->turn_delay+6000)
			{
				app->simple_font_yellow->print_left(10,440,"ENEMY RECOVERY AND MOVEMENT PHASE");
				app->damage_font_green->print_left(10,452,"The enemy recovery and movement phase has finished.");
				app->damage_font_green->print_left(10,462,"Press Continue to start a new turn.");

				continue_button->draw(1);

				if (continue_button->is_pressed())
				{
					app->game_state=g_recovery;
				}
			}

			break;

		case g_recovery:

			logo=CL_Surface::load("Graphics/Interface/hvastica",app->resources);
			logo->put_screen(494,435);
			app->simple_font_yellow->print_left(515,440,"Recovery phase");
			app->simple_font->print_left(515,456,CL_String("Turn: ")+CL_String(app->turn) + CL_String(" of ")+CL_String(app->scenario_turn));

			break;

		case end_scenario:

			if (app->turn>app->scenario_turn)
				app->menu->quit=true;	
			//or all the units are lost
			if (app->dead_units>=app->number_of_germans)
			{
				app->scenario_lost->put_screen(170,110);

				if (CL_Mouse::get_x()>170 && CL_Mouse::get_x()<470 &&
					CL_Mouse::get_y()>110 && CL_Mouse::get_y()<310 &&
					CL_Mouse::left_pressed())
				{
					app->menu->quit=true;
				}
			}

			break;
	}
}

void Interface::draw()
{	 
	scen_interface->put_screen(0,0);
	update();
	app->scenario_minimap->draw();
	app->scenario_minimap->update();
}


