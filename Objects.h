/***************************************************************************
                          Objects.h  -  description
                             -------------------
    copyright            : (C) 2000 by Kalman Andrasi
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#if _MSC_VER >= 1000
#pragma once
#endif 

#ifndef _OBJECTS_H_
#define _OBJECTS_H_
#define FC __fastcall

#include "map.h"

class AStar;
class projectile;

class GameObject
{
protected:

public:
	virtual ~GameObject() { return; };

	virtual float get_x() { return x; };
	virtual float get_y() { return y; };
	virtual int get_cp1_x() { return x+24; };		// checkpoints for collision with
	virtual int get_cp1_y() { return y+24; };		// other sprites and tiles. 

	struct STATE
	{
		int move;
		int attack;
		int guard;
		int duck;
	} state;

	virtual CL_ClipRect get_rect()
	{
		return CL_ClipRect(
		x,
		y,
		x+TILE_WIDTH,
		y+TILE_HEIGHT);
	}

	virtual bool tile_hit(Kursk* app)
	{
		if (app->scenario_map->drive_map[(int)y/TILE_HEIGHT][(int)x/TILE_WIDTH]=='#')
			return true;

		return false;
	}

	virtual int hit_check(GameObject* source,Kursk* app)=0;
	virtual void show(int X,int Y,Kursk* app)=0;
	virtual void update(Kursk* app)=0;
	virtual void move(Kursk* app)=0;

	virtual int calculate_dir(void)
	{
		//calculate the direction
		//going EAST
		if (right && !left && 
			!top && !bottom)
		{
			return 0;
		}
		//going WEST
		if (!right && left && 
			!top && !bottom)
		{
			return 4;
		}
		//going NORTH
		if (!right && !left && 
			top && !bottom)
		{
			return 6;
		}
		//going SOUTH
		if (!right && !left && 
			!top && bottom)
		{
			return 2;
		}
		//going SOUTH-WEST
		if (!right && left && 
			!top && bottom)
		{
			return 3;
		}
		//going NORTH-WEST
		if (!right && left && 
			top && !bottom)
		{
			return 5;
		}
		//going NORTH-EAST
		if (right && !left && 
			top && !bottom)
		{
			return 7;
		}
		//going SOUTH-EAST
		if (right && !left && 
			!top && bottom)
		{
			return 1;
		}
		return 0;
	}

	virtual int enemy_seen(Kursk* app)=0;

	AStar* unit_path;
	bool do_astar;
	bool do_fire;

	CL_Surface* hull;
	CL_Surface* turret;
	CL_Surface* shadow;
	CL_Surface* tmp_hull;
	CL_Surface* tmp_hull1;
	CL_Surface* tmp_hull2;
	CL_Surface* tmp_hull3;
	CL_Surface* tmp_hull4;
	CL_Surface* tmp_hull5;
	CL_Surface* tmp_hull6;
	CL_Surface* tmp_hull7;
	CL_Surface* tmp_hull8;
	CL_Surface* tmp_hull9;
	CL_Surface* tmp_turret;
	CL_Surface* tmp_shadow;
	CL_Surface* tmp_shadow1;
	CL_Surface* tmp_shadow2;
	CL_Surface* tmp_shadow3;
	CL_Surface* tmp_shadow4;
	CL_Surface* tmp_shadow5;
	CL_Surface* tmp_shadow6;
	CL_Surface* tmp_shadow7;
	CL_Surface* tmp_shadow8;
	CL_Surface* tmp_shadow9;
	CL_Surface* shadow_array[8];
	CL_Surface* shadow_array1[8];
	CL_Surface* shadow_array2[8];
	CL_Surface* shadow_array3[8];
	CL_Surface* shadow_array4[8];
	CL_Surface* shadow_array5[8];
	CL_Surface* shadow_array6[8];
	CL_Surface* shadow_array7[8];
	CL_Surface* shadow_array8[8];
	CL_Surface* shadow_array9[8];
	CL_Surface* turret_array[8];
	CL_Surface* hull_array[8];
	CL_Surface* hull_array1[8];
	CL_Surface* hull_array2[8];
	CL_Surface* hull_array3[8];
	CL_Surface* hull_array4[8];
	CL_Surface* hull_array5[8];
	CL_Surface* hull_array6[8];
	CL_Surface* hull_array7[8];
	CL_Surface* hull_array8[8];
	CL_Surface* hull_array9[8];
	CL_Surface* selection;
	CL_Surface* minimap_dot;
	CL_Surface* minimap_dot_w;
	CL_Surface* burning0;

	CL_SoundBuffer* move0;
	CL_SoundBuffer* weapon0;
	CL_SoundBuffer* cmgweapon0;
	CL_SoundBuffer* explosion0;
	
	projectile* shell;

	char object_type[25];
	//char* commander;
	CL_String commander;
	int hull_frame;
	int turret_frame;
	int object_id;						//identifies the type of the vehicle
	int object_index;					//unique vehicle identifier
	int object_status;
	int cp1_x,cp1_y;
	int size_xy;
	int direction;
	int visible_by;
	int burning_frame;
	float x,y;
	int to_hit[8];
	int penetration_value[80];
	int number_of_soldiers;
	int speed;

	int enemy_type[15];					//array to store the unit id's an enemy sees
	int enemy_x[15];
	int enemy_y[15];
	int enemy_hull_frame[15];
	int enemy_turret_frame[15];
	int enemy_fired_at;
	int enemy_index[15];
	int enemy_morale[15];

	int temp_x,temp_y;
	int target_x,target_y;
	int target_visibility;
	int enemy_number;
	int shot_maingun;
	int shot_coax;
	int movement_point;
	int suppressed;
	int leader_ability;
	int morale;
	int blt_priority;
	int burning_frame_delay;
	int combat_strength;
	int range;
	int shot_fired_at;
	int rally_count;
	bool is_turret;
	bool is_dead;
	bool is_selected;
	bool is_selectable;
	bool is_german;
	bool top,bottom,left,right;			//direction
	bool is_visible;
	bool is_firing;
	bool fired_at;						//the unit is a target
	bool is_burning;
	bool is_fired;						//during the enemy movement phase only one shot allowed
	bool is_moved;						//the ai moved the unit
	bool is_finished_firing;			//the unit finished the enemy firing phase
	bool is_dugin;
	bool draw_sandbag;					//a flag for drawing dug-in units
};

class GameObject_German : public GameObject
{
protected:

public:
	GameObject_German(int _x,int _y,int _object_id,int _object_index,Kursk* app);

	virtual int hit_check(GameObject* source,Kursk* app);
	virtual int enemy_seen(Kursk* app);

	virtual void show(int x,int y,Kursk* app);
	virtual void update(Kursk* app);
	virtual void move(Kursk* app);
};

class GameObject_Russian : public GameObject
{
protected:

public:
	GameObject_Russian(int _x,int _y,int _object_id,int _object_index,Kursk* app);

	virtual int hit_check(GameObject* source,Kursk* app);
	virtual int enemy_seen(Kursk* app);

	virtual void show(int x,int y,Kursk* app);
	virtual void update(Kursk* app);
	virtual void move(Kursk* app);
};

#undef FC
#endif