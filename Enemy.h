/***************************************************************************
                          Enemy.h  -  description
                             -------------------
    copyright            : (C) 2000 by Kalman Andrasi
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#if _MSC_VER >= 1000
#pragma once
#endif 

#ifndef _ENEMY_H_
#define _ENEMY_H_
#define FC __fastcall

class Enemy
{
public:
	Enemy(Kursk* app);

	static void enemy_ai(Kursk* app);
	static int enemy_movement(Kursk* app);
	static void initialize_influence_map(Kursk* app);
	static int generate_influence_map(Kursk* app);
};

#undef FC
#endif