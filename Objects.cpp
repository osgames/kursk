/***************************************************************************
                          Objects.cpp  -  description
                             -------------------
    copyright            : (C) 2000 by Kalman Andrasi
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include <time.h>
#include <fstream.h>
#include "Main.h"
#include "Objects.h"
#include "Minimap.h"
#include "Path.h"
#include "Projectile.h"
#include "Interface.h"
#include "Math.h"
#include "Enemy.h"
#include "Miscellaneous.h"
#include "caimagemanipulation.h"
#include "Smoke.h"

GameObject_German::GameObject_German(int _x,int _y,int _object_id,int _object_index,Kursk* app)
{
	int i;

	object_id=_object_id;
	object_index=_object_index;
	x=_x;
	y=_y;

	srand((unsigned)time(NULL));

	//initializing common variables
	is_dead=false;
	is_selected=false;
	is_german=true;
	object_status=0;
	do_astar=false;
	do_fire=false;
	is_visible=true;
	enemy_number=0;
	visible_by=255;
	is_firing=false;
	fired_at=false;
	suppressed=0;
	leader_ability=0;
	is_burning=false;
	burning_frame=0;
	blt_priority=0;
	shot_fired_at=0;
	is_dugin=false;
	draw_sandbag=false;
	rally_count=0;

	//loading the dot for the minimap
	minimap_dot=CL_Surface::load("Graphics/Objects/reddot",app->resources);
	minimap_dot_w=CL_Surface::load("Graphics/Objects/whitedot",app->resources);
	burning0=CL_Surface::load("Graphics/Objects/burning0",app->resources);
	selection=CL_Surface::load("Graphics/Objects/selection",app->resources);
	
	switch (object_id)
	{
		case 0:		//Pz IVJ
		{
			is_turret=true;
			is_selectable=true;
			tmp_hull=CL_Surface::load("Graphics/Objects/gpzivj",app->resources);
			tmp_turret=CL_Surface::load("Graphics/Objects/gpzivjt",app->resources);
			tmp_shadow=CL_Surface::load("Graphics/Objects/gpzivj_shadow",app->resources);
			for (i=0;i<8;i++)
			{
				hull_array[i]=CAImageManipulation::rotate(tmp_hull,(float)i/8*360.0,true);
				turret_array[i]=CAImageManipulation::rotate(tmp_turret,(float)i/8*360.0,true);
				shadow_array[i]=CAImageManipulation::rotate(tmp_shadow,(float)i/8*360.0,true);
			}
			move0=CL_SoundBuffer::load("Sound/tankmove0",app->resources);
			weapon0=CL_SoundBuffer::load("Sound/tankweapon0",app->resources);
			cmgweapon0=CL_SoundBuffer::load("Sound/tankcmgweapon0",app->resources);
			cp1_x=x+24;
			cp1_y=y+24;
			Math::load_data(this);
			break;
		}
		case 1:		//Heavy Weapons Squad
		{
			is_turret=false;
			is_selectable=true;
			tmp_hull=CL_Surface::load("Graphics/Objects/gsoldier",app->resources);
			tmp_hull1=CL_Surface::load("Graphics/Objects/gsoldier1",app->resources);
			tmp_hull2=CL_Surface::load("Graphics/Objects/gsoldier2",app->resources);
			tmp_hull3=CL_Surface::load("Graphics/Objects/gsoldier3",app->resources);
			tmp_hull4=CL_Surface::load("Graphics/Objects/gsoldier4",app->resources);
			tmp_hull5=CL_Surface::load("Graphics/Objects/gsoldier5",app->resources);
			tmp_hull6=CL_Surface::load("Graphics/Objects/gsoldier6",app->resources);
			tmp_hull7=CL_Surface::load("Graphics/Objects/gsoldier7",app->resources);
			tmp_hull8=CL_Surface::load("Graphics/Objects/gsoldier8",app->resources);
			tmp_shadow=CL_Surface::load("Graphics/Objects/gsoldier_shadow",app->resources);
			tmp_shadow1=CL_Surface::load("Graphics/Objects/gsoldier1_shadow",app->resources);
			tmp_shadow2=CL_Surface::load("Graphics/Objects/gsoldier2_shadow",app->resources);
			tmp_shadow3=CL_Surface::load("Graphics/Objects/gsoldier3_shadow",app->resources);
			tmp_shadow4=CL_Surface::load("Graphics/Objects/gsoldier4_shadow",app->resources);
			tmp_shadow5=CL_Surface::load("Graphics/Objects/gsoldier5_shadow",app->resources);
			tmp_shadow6=CL_Surface::load("Graphics/Objects/gsoldier6_shadow",app->resources);
			tmp_shadow7=CL_Surface::load("Graphics/Objects/gsoldier7_shadow",app->resources);
			tmp_shadow8=CL_Surface::load("Graphics/Objects/gsoldier8_shadow",app->resources);
			for (i=0;i<8;i++)
			{
				hull_array[i]=CAImageManipulation::rotate(tmp_hull,(float)i/8*360.0,true);
				hull_array1[i]=CAImageManipulation::rotate(tmp_hull1,(float)i/8*360.0,true);
				hull_array2[i]=CAImageManipulation::rotate(tmp_hull2,(float)i/8*360.0,true);
				hull_array3[i]=CAImageManipulation::rotate(tmp_hull3,(float)i/8*360.0,true);
				hull_array4[i]=CAImageManipulation::rotate(tmp_hull4,(float)i/8*360.0,true);
				hull_array5[i]=CAImageManipulation::rotate(tmp_hull5,(float)i/8*360.0,true);
				hull_array6[i]=CAImageManipulation::rotate(tmp_hull6,(float)i/8*360.0,true);
				hull_array7[i]=CAImageManipulation::rotate(tmp_hull7,(float)i/8*360.0,true);
				hull_array8[i]=CAImageManipulation::rotate(tmp_hull8,(float)i/8*360.0,true);
				shadow_array[i]=CAImageManipulation::rotate(tmp_shadow,(float)i/8*360.0,true);
				shadow_array1[i]=CAImageManipulation::rotate(tmp_shadow1,(float)i/8*360.0,true);
				shadow_array2[i]=CAImageManipulation::rotate(tmp_shadow2,(float)i/8*360.0,true);
				shadow_array3[i]=CAImageManipulation::rotate(tmp_shadow3,(float)i/8*360.0,true);
				shadow_array4[i]=CAImageManipulation::rotate(tmp_shadow4,(float)i/8*360.0,true);
				shadow_array5[i]=CAImageManipulation::rotate(tmp_shadow5,(float)i/8*360.0,true);
				shadow_array6[i]=CAImageManipulation::rotate(tmp_shadow6,(float)i/8*360.0,true);
				shadow_array7[i]=CAImageManipulation::rotate(tmp_shadow7,(float)i/8*360.0,true);
				shadow_array8[i]=CAImageManipulation::rotate(tmp_shadow8,(float)i/8*360.0,true);
			}
			weapon0=CL_SoundBuffer::load("Sound/infweapon0",app->resources);
			move0=CL_SoundBuffer::load("Sound/infmove0",app->resources);
			cp1_x=x+30;
			cp1_y=y+30;
			Math::load_data(this);
			break;
		}
		case 2:		//Marder
		{
			is_turret=false;
			is_selectable=true;
			tmp_hull=CL_Surface::load("Graphics/Objects/gmarderiii",app->resources);
			tmp_shadow=CL_Surface::load("Graphics/Objects/gmarderiii_shadow",app->resources);
			for (i=0;i<8;i++)
			{
				hull_array[i]=CAImageManipulation::rotate(tmp_hull,(float)i/8*360.0,true);
				shadow_array[i]=CAImageManipulation::rotate(tmp_shadow,(float)i/8*360.0,true);
			}
			move0=CL_SoundBuffer::load("Sound/tankmove0",app->resources);
			weapon0=CL_SoundBuffer::load("Sound/tankweapon0",app->resources);
			cmgweapon0=CL_SoundBuffer::load("Sound/tankcmgweapon0",app->resources);
			cp1_x=x+24;
			cp1_y=y+24;
			Math::load_data(this);
			break;
		}
		case 3:		//Medium Mortar Team
		{
			is_turret=false;
			is_selectable=true;
			tmp_hull=CL_Surface::load("Graphics/Objects/gmortar80_team",app->resources);
			tmp_shadow=CL_Surface::load("Graphics/Objects/gmortar80_team_shadow",app->resources);
			for (i=0;i<8;i++)
			{
				hull_array[i]=CAImageManipulation::rotate(tmp_hull,(float)i/8*360.0,true);
				shadow_array[i]=CAImageManipulation::rotate(tmp_shadow,(float)i/8*360.0,true);
			}
			explosion0=CL_SoundBuffer::load("Sound/mortarexplosion0",app->resources);
			cp1_x=x+24;
			cp1_y=y+24;
			Math::load_data(this);
			break;
		}
		case 4:		//Bailed-out crew
		{
			is_turret=false;
			is_selectable=false;
			Math::load_data(this);
			number_of_soldiers=1+rand()%4;
			if (number_of_soldiers==1)
			{
				tmp_hull=CL_Surface::load("Graphics/Objects/gsoldier1",app->resources);
				tmp_shadow=CL_Surface::load("Graphics/Objects/gsoldier1_shadow",app->resources);
				for (i=0;i<8;i++)
				{
					hull_array[i]=CAImageManipulation::rotate(tmp_hull,(float)i/8*360.0,true);
					shadow_array[i]=CAImageManipulation::rotate(tmp_shadow,(float)i/8*360.0,true);
				}
				hull=hull_array[4];
				shadow=shadow_array[4];
			}
			else if (number_of_soldiers==2)
			{
				tmp_hull=CL_Surface::load("Graphics/Objects/gsoldier2",app->resources);
				tmp_shadow=CL_Surface::load("Graphics/Objects/gsoldier2_shadow",app->resources);
				for (i=0;i<8;i++)
				{
					hull_array[i]=CAImageManipulation::rotate(tmp_hull,(float)i/8*360.0,true);
					shadow_array[i]=CAImageManipulation::rotate(tmp_shadow,(float)i/8*360.0,true);	
				}
				hull=hull_array[4];
				shadow=shadow_array[4];
			}
			else if (number_of_soldiers==3)
			{
				tmp_hull=CL_Surface::load("Graphics/Objects/gsoldier3",app->resources);
				tmp_shadow=CL_Surface::load("Graphics/Objects/gsoldier3_shadow",app->resources);
				for (i=0;i<8;i++)
				{
					hull_array[i]=CAImageManipulation::rotate(tmp_hull,(float)i/8*360.0,true);
					shadow_array[i]=CAImageManipulation::rotate(tmp_shadow,(float)i/8*360.0,true);
				}
				hull=hull_array[4];
				shadow=shadow_array[4];
			}
			else if (number_of_soldiers==4)
			{
				tmp_hull=CL_Surface::load("Graphics/Objects/gsoldier4",app->resources);
				tmp_shadow=CL_Surface::load("Graphics/Objects/gsoldier4_shadow",app->resources);
				for (i=0;i<8;i++)
				{
					hull_array[i]=CAImageManipulation::rotate(tmp_hull,(float)i/8*360.0,true);
					shadow_array[i]=CAImageManipulation::rotate(tmp_shadow,(float)i/8*360.0,true);
				}
				hull=hull_array[4];
				shadow=shadow_array[4];
			}
			move0=CL_SoundBuffer::load("Sound/infmove0",app->resources);
			cp1_x=x+30;
			cp1_y=y+30;
			break;
		}
	}
}

void GameObject_German::show(int X,int Y,Kursk* app)
{
	shadow->put_screen(X+8,Y+8);
	hull->put_screen(X,Y);
	if (is_turret)
		turret->put_screen(X,Y);
	if (is_selected)
		if (object_id==1 || object_id==4)
			selection->put_screen(X+6,Y+6);
		else
			selection->put_screen(X,Y);

	if (is_burning)
	{
		//if (object_id==2)
			burning0->put_screen(X-10,Y-30,burning_frame);
		//else
		//	burning0->put_screen(X-4,Y-24,burning_frame);

		if (CL_System::get_time()>=burning_frame_delay+65)
		{
			burning_frame++;
			burning_frame_delay=CL_System::get_time();
		}

		if (burning_frame>31)
			burning_frame=15;
	}
}

void GameObject_German::update(Kursk* app)
{
	//int dest_x,dest_y;
	float angle,die_roll;

	switch (app->game_state)
	{
	case manual_deployment:

		x=48;						//x and y coordinates of the object on the map
		temp_x=x;
		y=48+(96*object_index);
		temp_y=y;

		shadow=tmp_shadow;
		hull=tmp_hull;				//hull animation frame
		if (is_turret)				//same for the turret, if it has one...
			turret=tmp_turret; 

		break;

	case drag_and_drop:

		if ((CL_Mouse::get_x()+app->map_x >= x && CL_Mouse::get_x()+app->map_x <= x+size_xy) &&
			(CL_Mouse::get_y()+app->map_y >= y && CL_Mouse::get_y()+app->map_y <= y+size_xy) &&
			CL_Mouse::left_pressed() && is_german)
		{
			if (app->selected_count==1)
				is_selected=true;

			if (is_selected)
			{
				x=CL_Mouse::get_x()-size_xy/2+app->map_x;
				y=CL_Mouse::get_y()-size_xy/2+app->map_y;

				switch (object_id)
				{
				case 0:
					if (object_index==0)
						app->simple_font_yellow->print_center(x-app->map_x+size_xy/2,
						y-app->map_y-4,"Pz. IVJ Cmdr.");
					else
						app->simple_font_yellow->print_center(x-app->map_x+size_xy/2,
						y-app->map_y-4,"Pz. IVJ");
					break;
				case 1:
					app->simple_font_yellow->print_center(x-app->map_x+size_xy/2,
						y-app->map_y-8,"Hvy Wpns Sqd");
					break;
				case 2:
					app->simple_font_yellow->print_center(x-app->map_x+size_xy/2,
						y-app->map_y-10,"Marder IIIG");
					break;
				case 3:
					app->simple_font_yellow->print_center(x-app->map_x+size_xy/2,
						y-app->map_y-8,"Mrtr Team");
					break;
				}
			}
			app->selected_count++;
		}
		else if (!CL_Mouse::left_pressed())
		{
			app->selected_count=1;

			app->scenario_map->set_w_map_free((int)temp_x/TILE_WIDTH,(int)temp_y/TILE_HEIGHT);
			app->scenario_map->drive_map[(int)(temp_y+24)/TILE_HEIGHT][(int)(temp_x+24)/TILE_WIDTH]='|';

			x=((int)x/TILE_WIDTH)*TILE_WIDTH;
			y=((int)y/TILE_HEIGHT)*TILE_HEIGHT;

			is_selected=false;

			//check if the unit is on a black tile
			if (app->scenario_map->get_w_map((int)x/TILE_WIDTH,(int)y/TILE_HEIGHT)==0)
			{
				x=temp_x;
				y=temp_y;
			}
			else
			{
				temp_x=x;
				temp_y=y;
				app->scenario_map->set_w_map((int)x/TILE_WIDTH,(int)y/TILE_HEIGHT);
				app->scenario_map->drive_map[(int)(y+24)/TILE_HEIGHT][(int)(x+24)/TILE_WIDTH]='#';
			}
		}
		break;

	case g_movement:

		if ((CL_Mouse::get_x()+app->map_x>=x && CL_Mouse::get_x()+app->map_x<=x+size_xy) &&
			(CL_Mouse::get_y()+app->map_y>=y && CL_Mouse::get_y()+app->map_y<=y+size_xy) &&
			CL_Mouse::left_pressed() && is_german && is_selectable)
		{
			//clear all selection flags
			{for (std::list<GameObject*>::iterator counter=app->objects.begin();counter!=app->objects.end();counter++)
				(*counter)->is_selected=false;
			}

			is_selected=true;
		}

		if (CL_Mouse::middle_pressed())
		{
			//initializing unit pathfinding
			if (is_selected  && movement_point > 0)
			{
				app->scenario_map->drive_map[(int)(y+24)/TILE_HEIGHT][(int)(x+24)/TILE_WIDTH]='|';

				unit_path=new AStar(app->scenario_map);
				do_astar=true;

				//sound effects
				if (!move0->is_playing())
					move0->play(true);
			}

			target_x=((CL_Mouse::get_x()+app->map_x)/TILE_WIDTH)*TILE_WIDTH;
			target_y=((CL_Mouse::get_y()+app->map_y)/TILE_HEIGHT)*TILE_HEIGHT;

		}

		move(app);	//do some pathfinding

		//enemy reaction in the movement phase
		//************************************
		Enemy::enemy_ai(app);

		break;

	case rally:

		srand((unsigned)time(NULL));

		//rally suppressed units
		if (suppressed>0 && object_status!=7 &&
			is_selected && rally_count<3)
		{
			die_roll=rand()%12;

			if (die_roll<=morale)
			{
				suppressed--;
				app->scenario_interface->message1_target_type="Successful rally.";
				app->game_state=continue_game;
			}
			else
			{
				rally_count++;
				app->scenario_interface->message1_target_type="Unsuccessful rallying.";
				rally_count=3;
				app->game_state=continue_game;
			}
		}
		else if (rally_count>=3 && is_selected)
		{
			app->scenario_interface->message1_target_type="The rally quota is exceeded.";
			app->game_state=continue_game;
		}
		else if (suppressed==0 && is_selected)
		{
			app->scenario_interface->message1_target_type="This unit is not suppressed!";
			app->game_state=continue_game;
		}

		break;

	case g_fire:
		//reset the interface messaging
		app->scenario_interface->message1_target_type="";
		app->scenario_interface->message2_target_distance="";
		app->scenario_interface->message3_hit_chance="";

		if ((CL_Mouse::get_x()+app->map_x>=x && CL_Mouse::get_x()+app->map_x<=x+size_xy) &&
			(CL_Mouse::get_y()+app->map_y>=y && CL_Mouse::get_y()+app->map_y<=y+size_xy) &&
			CL_Mouse::left_pressed() && is_german && is_selectable)
		{
			//clear all selection flags
			{for (std::list<GameObject*>::iterator counter=app->objects.begin();counter!=app->objects.end();counter++)
				(*counter)->is_selected=false;	
			}

			is_selected=true;
			is_firing=false;
		}

		{for (std::list<GameObject*>::iterator it=app->objects.begin();it!=app->objects.end();it++)

			if ((*it)->is_selected)
			{
				{for (std::list<GameObject*>::iterator item=app->objects.begin();item!=app->objects.end();item++)

					if (CL_Mouse::get_x()+app->map_x>=(*item)->x && 
						CL_Mouse::get_x()+app->map_x<=(*item)->x+(*item)->size_xy &&
						CL_Mouse::get_y()+app->map_y>=(*item)->y && 
						CL_Mouse::get_y()+app->map_y<=(*item)->y+(*item)->size_xy &&
						!(*item)->is_german && (*item)->visible_by==(*it)->object_index &&
						((*it)->shot_maingun+(*it)->shot_coax) != 0 && (*it)->object_id!=3 &&
						!(*item)->is_dead)
					{
						//give the target type and distance to interface messaging
						app->scenario_interface->message1_target_type=CL_String((*item)->object_type);
						app->scenario_interface->message2_target_distance=CL_String(
							Math::distance((*it)->x,(*it)->y,(*item)->x,(*item)->y)*50);

						if (CL_Mouse::middle_pressed())
						{
							(*it)->shell=new projectile(app,(*it),(*item));
							(*it)->is_firing=true;
							(*it)->blt_priority=1;
							(*item)->fired_at=true;

							//smoke effects routine
							(*item)->shot_fired_at++;
							if ((*item)->shot_fired_at > 3)
							{
								app->smoke_number++;
								app->smoke[app->smoke_number]=new Smoke(app);
								app->smoke[app->smoke_number]->smoke_x=(*item)->x;
								app->smoke[app->smoke_number]->smoke_y=(*item)->y;
								(*item)->shot_fired_at=0;				
							}

							//sound effects
							if ((*it)->object_id==0 || (*it)->object_id==2)
								if ((*it)->shot_maingun > 0)
									(*it)->weapon0->play();
								else
									(*it)->cmgweapon0->play();
							else 
								(*it)->weapon0->play();
							

							app->scenario_interface->message4_hit_results="";
							app->scenario_interface->message5_hit_results="";
							app->scenario_interface->message6_hit_results="";

							//decrease the number of available shots
							if ((*it)->object_id != 1)
								if ((*it)->shot_maingun > 0)
									(*it)->shot_maingun--;
								else
									(*it)->shot_coax--;

							else
								(*it)->shot_coax--;

							//calculate the result of firing
							Analysis::target_eliminated((*it),(*item),app);


                            //turn the turret, if any 
                            angle=atan2((*it)->shell->dest_y-(*it)->shell->start_y,
                                (*it)->shell->dest_x-(*it)->shell->start_x);
							
							if ((*it)->is_turret)
								(*it)->turret=CAImageManipulation::rotate((*it)->tmp_turret,(float)RADtoDEG(angle),true);
							else if ((*it)->object_id==1)
							{
								switch ((*it)->number_of_soldiers)
								{
								case 1:
									(*it)->shadow=CAImageManipulation::rotate((*it)->tmp_shadow1,(float)RADtoDEG(angle),true);
									(*it)->hull=CAImageManipulation::rotate((*it)->tmp_hull1,(float)RADtoDEG(angle),true);
									break;
								case 2:
									(*it)->shadow=CAImageManipulation::rotate((*it)->tmp_shadow2,(float)RADtoDEG(angle),true);
									(*it)->hull=CAImageManipulation::rotate((*it)->tmp_hull2,(float)RADtoDEG(angle),true);
									break;
								case 3:
									(*it)->shadow=CAImageManipulation::rotate((*it)->tmp_shadow3,(float)RADtoDEG(angle),true);
									(*it)->hull=CAImageManipulation::rotate((*it)->tmp_hull3,(float)RADtoDEG(angle),true);
									break;
								case 4:
									(*it)->shadow=CAImageManipulation::rotate((*it)->tmp_shadow4,(float)RADtoDEG(angle),true);
									(*it)->hull=CAImageManipulation::rotate((*it)->tmp_hull4,(float)RADtoDEG(angle),true);
									break;
								case 5:
									(*it)->shadow=CAImageManipulation::rotate((*it)->tmp_shadow5,(float)RADtoDEG(angle),true);
									(*it)->hull=CAImageManipulation::rotate((*it)->tmp_hull5,(float)RADtoDEG(angle),true);
									break;
								case 6:
									(*it)->shadow=CAImageManipulation::rotate((*it)->tmp_shadow6,(float)RADtoDEG(angle),true);
									(*it)->hull=CAImageManipulation::rotate((*it)->tmp_hull6,(float)RADtoDEG(angle),true);
									break;
								case 7:
									(*it)->shadow=CAImageManipulation::rotate((*it)->tmp_shadow7,(float)RADtoDEG(angle),true);
									(*it)->hull=CAImageManipulation::rotate((*it)->tmp_hull7,(float)RADtoDEG(angle),true);
									break;
								case 8:
									(*it)->shadow=CAImageManipulation::rotate((*it)->tmp_shadow8,(float)RADtoDEG(angle),true);
									(*it)->hull=CAImageManipulation::rotate((*it)->tmp_hull8,(float)RADtoDEG(angle),true);
									break;
								case 9:
									(*it)->shadow=CAImageManipulation::rotate((*it)->tmp_shadow,(float)RADtoDEG(angle),true);
									(*it)->hull=CAImageManipulation::rotate((*it)->tmp_hull,(float)RADtoDEG(angle),true);
									break;
								}
							}
							else if ((*it)->object_id==2)
							{
								(*it)->shadow=CAImageManipulation::rotate((*it)->tmp_shadow,(float)RADtoDEG(angle),true);
								(*it)->hull=CAImageManipulation::rotate((*it)->tmp_hull,(float)RADtoDEG(angle),true);
							}

                            app->game_state=g_do_fire;

                            break;
						}
					}
					else if ((*it)->object_id==3 && 
							CL_Mouse::get_x()+app->map_x>=(*item)->x && 
							CL_Mouse::get_x()+app->map_x<=(*item)->x+(*item)->size_xy &&
							CL_Mouse::get_y()+app->map_y>=(*item)->y && 
							CL_Mouse::get_y()+app->map_y<=(*item)->y+(*item)->size_xy &&
							!(*item)->is_german && !(*item)->is_dead)
					{
						//give the target type and distance to interface messaging
						app->scenario_interface->message1_target_type=CL_String((*item)->object_type);
						app->scenario_interface->message2_target_distance=CL_String(
							Math::distance((*it)->x,(*it)->y,(*item)->x,(*item)->y)*50);

						if (CL_Mouse::middle_pressed() && (*it)->shot_maingun+(*it)->shot_coax != 0)
						{
							(*it)->shell=new projectile(app,(*it),(*item));
							(*it)->is_firing=true;
							(*it)->blt_priority=1;
							(*item)->fired_at=true;
							
							//smoke effects routine
							(*item)->shot_fired_at++;
							if ((*item)->shot_fired_at > 3)
							{
								app->smoke_number++;
								app->smoke[app->smoke_number]=new Smoke(app);
								app->smoke[app->smoke_number]->smoke_x=(*item)->x;
								app->smoke[app->smoke_number]->smoke_y=(*item)->y;
								(*item)->shot_fired_at=0;
							}

							(*it)->explosion0->play();

							app->crater_visible=false;

							app->scenario_interface->message4_hit_results="";
							app->scenario_interface->message5_hit_results="";
							app->scenario_interface->message6_hit_results="";
						
							//decrease the number of available shots
							if ((*it)->shot_maingun > 0)
								(*it)->shot_maingun--;
							else
								(*it)->shot_coax--;

							app->game_state=g_do_fire;

							//calculate the result of firing
							Analysis::target_eliminated((*it),(*item),app);

							break;
						}
					}
				}
				break;
			}
		}

		break;

	case g_do_fire:

		{for (std::list<GameObject*>::iterator it=app->objects.begin();it!=app->objects.end();it++)

			if ((*it)->is_firing)
			{
				if (!(*it)->shell->is_dead)
				{
					(*it)->shell->update(app);
					(*it)->shell->show(app);
				}
				else
				{
					if ((*it)->shell->frame_counter<12)
					{
						(*it)->shell->explosion->put_screen((*it)->shell->dest_x-TILE_WIDTH/2-app->map_x,
                            (*it)->shell->dest_y-TILE_HEIGHT/2-app->map_y,(*it)->shell->frame_counter);

						if (CL_System::get_time()>=(*it)->shell->projectile_frame_delay+65)
						{
							(*it)->shell->frame_counter++;
							(*it)->shell->projectile_frame_delay=CL_System::get_time();
						}						
					}
					else
					{
						(*it)->is_firing=false;
						(*it)->blt_priority=0;
						app->crater_visible=true;
						app->game_state=g_fire;

						break;
					}
				}
			}
		}

		break;

	case g_recovery:

		srand((unsigned)time(NULL));
		app->russian_turn=false;
		
		{for (std::list<GameObject*>::iterator it=app->objects.begin();it!=app->objects.end();it++)

			if ((*it)->is_german && !(*it)->is_dead)
			{
				//reset the rally counter
				(*it)->rally_count=0;

				//rally suppressed units
				if ((*it)->suppressed>0 && (*it)->object_status!=7)
				{
					for (int i=1;i<=(*it)->suppressed;i++)
					{
						die_roll=rand()%12;

						if (die_roll<=(*it)->morale)
							(*it)->suppressed--;
					}
					//if it's still suppressed after rallying
					if ((*it)->suppressed>0)
					{
						(*it)->shot_maingun=0;
						(*it)->shot_coax=0;
						(*it)->movement_point=0;
						(*it)->object_status=3;
					}
				}
				//rally pinned and routed units
				else if ((*it)->object_status==2 || (*it)->object_status==3 ||
					(*it)->object_status==8)
				{
					die_roll=rand()%12;

					if (die_roll<=(*it)->morale)
					{
						switch ((*it)->object_id)
						{
							case 0:			//Pz.IVJ
								(*it)->shot_maingun=3;
								(*it)->shot_coax=2;
								(*it)->movement_point=288;
								(*it)->object_status=0;
								(*it)->combat_strength=10;
								break;
							case 1:			//Infantry
								(*it)->shot_maingun=0;
								(*it)->shot_coax=3;
								(*it)->movement_point=240;
								(*it)->object_status=0;
								(*it)->combat_strength=6;
								break;
							case 2:			//Marder IIIG
								(*it)->shot_maingun=2;
								(*it)->shot_coax=3;
								(*it)->movement_point=288;
								(*it)->object_status=0;
								(*it)->combat_strength=7;
								break;
							case 3:			//8cm Mortar
								(*it)->shot_maingun=6;
								(*it)->shot_coax=0;
								(*it)->movement_point=0;
								(*it)->object_status=0;
								(*it)->combat_strength=3;
								break;
							case 4:			//crew
								(*it)->shot_maingun=0;
								(*it)->shot_coax=0;
								(*it)->movement_point=144;
								(*it)->object_status=0;
								(*it)->combat_strength=1;
								break;
						}
					}
					else
					{
						(*it)->shot_maingun=0;
						(*it)->shot_coax=0;
						(*it)->movement_point=0;
						(*it)->object_status=3;
					}
				}
				//repair damaged units
				else if ((*it)->object_status==1)		//gun malfunction
				{
					die_roll=rand()%20;
					//unit is repaired
					if (die_roll>=1 && die_roll<=4)
					{
						switch ((*it)->object_id)
						{
							case 0:			//Pz.IVJ
								(*it)->shot_maingun=3;
								(*it)->shot_coax=2;
								(*it)->movement_point=288;
								(*it)->object_status=0;
								(*it)->combat_strength=10;
								break;
							case 2:			//Marder IIIG
								(*it)->shot_maingun=2;
								(*it)->shot_coax=3;
								(*it)->movement_point=288;
								(*it)->object_status=0;
								(*it)->combat_strength=7;
								break;
							case 3:			//80mm Mortar
								(*it)->shot_maingun=6;
								(*it)->shot_coax=0;
								(*it)->movement_point=0;
								(*it)->object_status=0;
								(*it)->combat_strength=3;
								break;							
						}
					}
					else
					{
						(*it)->shot_maingun=0;
						(*it)->shot_coax=0;
						(*it)->movement_point=0;	
						(*it)->object_status=3;
					}
				}
				else if ((*it)->object_status==7)		//immobilized
				{
					(*it)->shot_maingun=1;
					(*it)->shot_coax=1;
					(*it)->movement_point=0;	
					(*it)->object_status=7;
					(*it)->combat_strength=3;

					//bail-out check
					die_roll=rand()%12;
					//if fails the crew is bailed out
					if (die_roll>(*it)->morale)
					{
						(*it)->is_dead=true;
						(*it)->object_status=4;			//abandoned
						(*it)->movement_point=0;
						(*it)->move0->stop();
						(*it)->enemy_number=0;
						(*it)->suppressed=0;
						(*it)->combat_strength=0;
						app->number_of_germans++;				
						app->objects.push_back(new GameObject_German((*it)->x-60,(*it)->y,4,app->number_of_germans,app));
						app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x-84)/TILE_WIDTH]='#';
					}
				}
				//these units are OK
				else if ((*it)->object_status==0)
				{
					switch ((*it)->object_id)
					{
						case 0:			//Pz.IVJ
							(*it)->shot_maingun=3;
							(*it)->shot_coax=2;
							(*it)->movement_point=288;
							(*it)->object_status=0;
							(*it)->combat_strength=10;
							break;
						case 1:			//Infantry
							(*it)->shot_maingun=0;
							(*it)->shot_coax=3;
							(*it)->movement_point=240;
							(*it)->object_status=0;
							(*it)->combat_strength=6;
							break;
						case 2:			//Marder IIIG
							(*it)->shot_maingun=2;
							(*it)->shot_coax=3;
							(*it)->movement_point=288;
							(*it)->object_status=0;
							(*it)->combat_strength=7;
							break;
						case 3:			//80mm Mortar
							(*it)->shot_maingun=6;
							(*it)->shot_coax=0;
							(*it)->movement_point=0;
							(*it)->object_status=0;
							(*it)->combat_strength=3;
							break;
						case 4:			//crew
							(*it)->shot_maingun=0;
							(*it)->shot_coax=0;
							(*it)->movement_point=144;
							(*it)->object_status=0;
							(*it)->combat_strength=1;
							break;
					}
				}
			}
		}
		//move bailed-out crews and routed units
		{for (std::list<GameObject*>::iterator it=app->objects.begin();it!=app->objects.end();it++)
		//bailed-out crews are moving off map
		//routed units and tanks with damaged main gun as well 
			if ((*it)->is_german && !(*it)->is_dead && !(*it)->is_moved &&
				((*it)->object_id==4 || (*it)->object_status==1 ||
				(*it)->object_status==3))
			{
				if ((*it)->object_id==1)
				{
					switch ((*it)->number_of_soldiers)
					{
					case 1:
						(*it)->shadow=(*it)->shadow_array1[4];
						(*it)->hull=(*it)->hull_array1[4];
						break;
					case 2:
						(*it)->shadow=(*it)->shadow_array2[4];
						(*it)->hull=(*it)->hull_array2[4];
						break;
					case 3:
						(*it)->shadow=(*it)->shadow_array3[4];
						(*it)->hull=(*it)->hull_array3[4];
						break;
					case 4:
						(*it)->shadow=(*it)->shadow_array4[4];
						(*it)->hull=(*it)->hull_array4[4];
						break;
					case 5:
						(*it)->shadow=(*it)->shadow_array5[4];
						(*it)->hull=(*it)->hull_array5[4];
						break;
					case 6:
						(*it)->shadow=(*it)->shadow_array6[4];
						(*it)->hull=(*it)->hull_array6[4];
						break;
					case 7:
						(*it)->shadow=(*it)->shadow_array7[4];
						(*it)->hull=(*it)->hull_array7[4];
						break;
					case 8:
						(*it)->shadow=(*it)->shadow_array8[4];
						(*it)->hull=(*it)->hull_array8[4];
						break;
					case 9:
						(*it)->shadow=(*it)->shadow_array[4];
						(*it)->hull=(*it)->hull_array[4];
						break;
					}
				}
				else
				{
					(*it)->shadow=(*it)->shadow_array[4];
					(*it)->hull=(*it)->hull_array[4];
				}
				if ((*it)->is_turret)
					(*it)->turret=(*it)->turret_array[4];
				app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='|';
				(*it)->x=(*it)->x-96;
				//check if the tile is free
				if (app->scenario_map->get_drive_dir((*it)->x/TILE_WIDTH,(*it)->y/TILE_HEIGHT)=='#')
				{
					(*it)->x=(*it)->x-48;
				}
				app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='#';
				//crew reached the edge of the map
				if ((*it)->x<=0)
				{
					(*it)->is_visible=false;
					(*it)->object_status=9;		//out of this scenario
				}

				(*it)->is_moved=true;
			}
		}

		if (app->g_is_artillery)
			app->game_state=g_artillery;
		else
		{
			{for (std::list<GameObject*>::iterator it=app->objects.begin();it!=app->objects.end();it++)
				(*it)->is_moved=false;
			}
			app->turn++;
			app->game_state=g_movement;
		}

		break;
	}
}

int GameObject_German::hit_check(GameObject* source,Kursk* app)
{
	CL_ClipRect source_rect=source->get_rect();

	{for (std::list<GameObject*>::iterator it=app->objects.begin();it!=app->objects.end();it++)
		//don't check yourself
		if ((*it)!=source) 
		{
			CL_ClipRect target_rect=(*it)->get_rect();
			if (source_rect.test_all_clipped(target_rect)==false)
			{
				//std::cout << "Collision!" << std::endl;
				return (1);
			}
		}
	}
	return (0);
}

GameObject_German::enemy_seen(Kursk* app)
{
	enemy_number=0;

	{for (std::list<GameObject*>::iterator it=app->objects.begin();it!=app->objects.end();it++)
		//don,t check itself	
		if (!(*it)->is_german)
		{
			//reset the visible_by variable, which stores the object_index
			(*it)->visible_by=255;

			//check for visibility distance
			if (Math::distance(x,y,(*it)->x,(*it)->y) <= target_visibility)
			{
				//next check for obstacles in LOS and don't count wrecked units
				if(!Math::obstacle_height(app,x,y,(*it)->x,(*it)->y) &&
					!(*it)->is_dead)
				{
					if (!(*it)->is_visible)
						(*it)->is_visible=true;
					//if the object is dug-in draw the sandbags as well
					if ((*it)->is_dugin)
						(*it)->draw_sandbag=true;

					//which unit sees the target
					(*it)->visible_by=object_index;
					enemy_number++;
				}
			}
		}
	}

	return (enemy_number);
}
void GameObject_German::move(Kursk* app)
{
	int dest_x,dest_y;

	if (do_astar)
	{
		if	(unit_path->NewPath(x,y,target_x,target_y))
		{
			left=right=top=bottom=false;

			app->scenario_map->drive_map[(int)((temp_y+24)/TILE_HEIGHT)][(int)((temp_x+24)/TILE_WIDTH)]='|';

			unit_path->PathNextNode();
			dest_x=unit_path->NodeGetX();
			dest_y=unit_path->NodeGetY();

			if (!unit_path->ReachedGoal() && dest_x==x && dest_y==y)
			{
				unit_path->PathNextNode();
				dest_x=unit_path->NodeGetX();
				dest_y=unit_path->NodeGetY();
			}
			///////////////////////////////////////////////////////
			if (dest_x > x)
			{
				x+=speed;
				right=true;

				if (tile_hit(app))
				{
					x-=speed;
					left=true;
				}
				movement_point-=speed;
			}
			if (dest_y > y)
			{
				y+=speed;
				bottom=true;

				if (tile_hit(app))
				{
					y-=speed;
					top=true;
				}
				movement_point-=speed;
			}
			if (dest_x < x)
			{
				x-=speed;
				left=true;

				if (tile_hit(app))
				{
					x+=speed;
					right=true;
				}
				movement_point-=speed;
			}
			if (dest_y < y)
			{
				y-=speed;
				top=true;

				if (tile_hit(app))
				{
					y+=speed;
					bottom=true;
				}
				movement_point-=speed;
			}
			//calculate the direction of movement
			if (is_turret)
			{
				shadow=shadow_array[calculate_dir()];
				hull=hull_array[calculate_dir()];
				turret=turret_array[calculate_dir()];
			}
			else if (object_id==1)
			{
				switch (number_of_soldiers)
				{
				case 1:
					shadow=shadow_array1[calculate_dir()];
					hull=hull_array1[calculate_dir()];
					break;
				case 2:
					shadow=shadow_array2[calculate_dir()];
					hull=hull_array2[calculate_dir()];
					break;
				case 3:
					shadow=shadow_array3[calculate_dir()];
					hull=hull_array3[calculate_dir()];
					break;
				case 4:
					shadow=shadow_array4[calculate_dir()];
					hull=hull_array4[calculate_dir()];
					break;
				case 5:
					shadow=shadow_array5[calculate_dir()];
					hull=hull_array5[calculate_dir()];
					break;
				case 6:
					shadow=shadow_array6[calculate_dir()];
					hull=hull_array6[calculate_dir()];
					break;
				case 7:
					shadow=shadow_array7[calculate_dir()];
					hull=hull_array7[calculate_dir()];
					break;
				case 8:
					shadow=shadow_array8[calculate_dir()];
					hull=hull_array8[calculate_dir()];
					break;
				case 9:
					shadow=shadow_array[calculate_dir()];
					hull=hull_array[calculate_dir()];
					break;
				}
			}
			else if (object_id==2)
			{
				shadow=shadow_array[calculate_dir()];
				hull=hull_array[calculate_dir()];
			}
			//check if the unit is on a victory location
			if (get_cp1_x()>=1728 && get_cp1_x()<=1776 && 
				get_cp1_y()>=192 && get_cp1_y()<=240)
			{
				app->scenario_map->victory_flag1=1;
			}
			else if (get_cp1_x()>=1488 && get_cp1_x()<=1536 && 
				get_cp1_y()>=624 && get_cp1_y()<=672)
			{
				app->scenario_map->victory_flag2=1;
			}
			else if (get_cp1_x()>=1824 && get_cp1_x()<=1872 && 
				get_cp1_y()>=1104 && get_cp1_y()<=1152)
			{
				app->scenario_map->victory_flag3=1;
			}
			if (movement_point<=0)
			{
				//run out of movement points
				is_selected=false;
				do_astar=false;
				move0->stop();
				delete unit_path;

				temp_x=x;
				temp_y=y;
				app->scenario_map->drive_map[(int)((y+24)/TILE_HEIGHT)][(int)((x+24)/TILE_WIDTH)]='#';				
			}
		}
		else if (is_selected)
		{
			//goal is reached!
			is_selected=false;
			do_astar=false;
			delete unit_path;
			move0->stop();

			temp_x=x;
			temp_y=y;
			app->scenario_map->drive_map[(int)((y+24)/TILE_HEIGHT)][(int)((x+24)/TILE_WIDTH)]='#';				
		}
	}
}

GameObject_Russian::GameObject_Russian(int _x,int _y,int _object_id,int _object_index,Kursk* app)
{
	int i;

	object_id=_object_id;
	object_index=_object_index;
	x=_x;
	y=_y;

	//initializing common variables
	object_status=0;
	is_german=false;
	is_dead=false;
	do_astar=false;
	do_fire=false;
	is_selected=false;
	is_selectable=false;
	is_visible=false;
	enemy_number=0;
	visible_by=255;
	is_firing=false;
	fired_at=false;
	suppressed=0;
	leader_ability=0;
	is_burning=false;
	burning_frame=0;
	blt_priority=0;
	is_fired=false;
	is_moved=false;
	is_finished_firing=false;
	shot_fired_at=0;
	is_dugin=false;
	draw_sandbag=false;
	rally_count=0;

	//loading the dot for the minimap
	minimap_dot=CL_Surface::load("Graphics/Objects/bluedot",app->resources);
	burning0=CL_Surface::load("Graphics/Objects/burning0",app->resources);

	switch (object_id)
	{
	case 0:		//T-34
		is_turret=true;
		tmp_hull=CL_Surface::load("Graphics/Objects/rt34",app->resources);
		tmp_turret=CL_Surface::load("Graphics/Objects/rt34t",app->resources);
		tmp_shadow=CL_Surface::load("Graphics/Objects/rt34_shadow",app->resources);
		for (i=0;i<8;i++)
		{
			hull_array[i]=CAImageManipulation::rotate(tmp_hull,(float)i/8*360.0,true);
			turret_array[i]=CAImageManipulation::rotate(tmp_turret,(float)i/8*360.0,true);
			shadow_array[i]=CAImageManipulation::rotate(tmp_shadow,(float)i/8*360.0,true);
		}	
		move0=CL_SoundBuffer::load("Sound/tankmove0",app->resources);
		weapon0=CL_SoundBuffer::load("Sound/tankweapon0",app->resources);
		cmgweapon0=CL_SoundBuffer::load("Sound/tankcmgweapon0",app->resources);
		cp1_x=x+24;
		cp1_y=y+24;
		Math::load_data(this);
		break;
	case 1:		//T-70
		is_turret=true;
		tmp_hull=CL_Surface::load("Graphics/Objects/rt70",app->resources);
		tmp_turret=CL_Surface::load("Graphics/Objects/rt70t",app->resources);
		tmp_shadow=CL_Surface::load("Graphics/Objects/rt70_shadow",app->resources);
		for (i=0;i<8;i++)
		{
			hull_array[i]=CAImageManipulation::rotate(tmp_hull,(float)i/8*360.0,true);
			turret_array[i]=CAImageManipulation::rotate(tmp_turret,(float)i/8*360.0,true);
			shadow_array[i]=CAImageManipulation::rotate(tmp_shadow,(float)i/8*360.0,true);
		}
		move0=CL_SoundBuffer::load("Sound/tankmove0",app->resources);
		weapon0=CL_SoundBuffer::load("Sound/tankweapon1",app->resources);
		cmgweapon0=CL_SoundBuffer::load("Sound/tankcmgweapon0",app->resources);
		cp1_x=x+24;
		cp1_y=y+24;
		Math::load_data(this);
		break;
	case 2:		//120mm mortar
		is_turret=false;
		tmp_hull=CL_Surface::load("Graphics/Objects/rmortar120",app->resources);
		tmp_shadow=CL_Surface::load("Graphics/Objects/rmortar120_shadow",app->resources);
		for (i=0;i<8;i++)
		{
			hull_array[i]=CAImageManipulation::rotate(tmp_hull,(float)i/8*360.0,true);
			shadow_array[i]=CAImageManipulation::rotate(tmp_shadow,(float)i/8*360.0,true);

		}
		cp1_x=x+24;
		cp1_y=y+24;
		Math::load_data(this);
		break;
	case 3:		//Infantry Sqd
		is_turret=false;
		tmp_hull=CL_Surface::load("Graphics/Objects/rsoldier",app->resources);
		tmp_hull1=CL_Surface::load("Graphics/Objects/rsoldier1",app->resources);
		tmp_hull2=CL_Surface::load("Graphics/Objects/rsoldier2",app->resources);
		tmp_hull3=CL_Surface::load("Graphics/Objects/rsoldier3",app->resources);
		tmp_hull4=CL_Surface::load("Graphics/Objects/rsoldier4",app->resources);
		tmp_hull5=CL_Surface::load("Graphics/Objects/rsoldier5",app->resources);
		tmp_hull6=CL_Surface::load("Graphics/Objects/rsoldier6",app->resources);
		tmp_hull7=CL_Surface::load("Graphics/Objects/rsoldier7",app->resources);
		tmp_hull8=CL_Surface::load("Graphics/Objects/rsoldier8",app->resources);
		tmp_hull9=CL_Surface::load("Graphics/Objects/rsoldier9",app->resources);
		tmp_shadow=CL_Surface::load("Graphics/Objects/rsoldier_shadow",app->resources);
		tmp_shadow1=CL_Surface::load("Graphics/Objects/rsoldier1_shadow",app->resources);
		tmp_shadow2=CL_Surface::load("Graphics/Objects/rsoldier2_shadow",app->resources);
		tmp_shadow3=CL_Surface::load("Graphics/Objects/rsoldier3_shadow",app->resources);
		tmp_shadow4=CL_Surface::load("Graphics/Objects/rsoldier4_shadow",app->resources);
		tmp_shadow5=CL_Surface::load("Graphics/Objects/rsoldier5_shadow",app->resources);
		tmp_shadow6=CL_Surface::load("Graphics/Objects/rsoldier6_shadow",app->resources);
		tmp_shadow7=CL_Surface::load("Graphics/Objects/rsoldier7_shadow",app->resources);
		tmp_shadow8=CL_Surface::load("Graphics/Objects/rsoldier8_shadow",app->resources);
		tmp_shadow9=CL_Surface::load("Graphics/Objects/rsoldier9_shadow",app->resources);
		for (i=0;i<8;i++)
		{
			hull_array[i]=CAImageManipulation::rotate(tmp_hull,(float)i/8*360.0,true);
			hull_array1[i]=CAImageManipulation::rotate(tmp_hull1,(float)i/8*360.0,true);
			hull_array2[i]=CAImageManipulation::rotate(tmp_hull2,(float)i/8*360.0,true);
			hull_array3[i]=CAImageManipulation::rotate(tmp_hull3,(float)i/8*360.0,true);
			hull_array4[i]=CAImageManipulation::rotate(tmp_hull4,(float)i/8*360.0,true);
			hull_array5[i]=CAImageManipulation::rotate(tmp_hull5,(float)i/8*360.0,true);
			hull_array6[i]=CAImageManipulation::rotate(tmp_hull6,(float)i/8*360.0,true);
			hull_array7[i]=CAImageManipulation::rotate(tmp_hull7,(float)i/8*360.0,true);
			hull_array8[i]=CAImageManipulation::rotate(tmp_hull8,(float)i/8*360.0,true);
			hull_array9[i]=CAImageManipulation::rotate(tmp_hull9,(float)i/8*360.0,true);
			shadow_array[i]=CAImageManipulation::rotate(tmp_shadow,(float)i/8*360.0,true);
			shadow_array1[i]=CAImageManipulation::rotate(tmp_shadow1,(float)i/8*360.0,true);
			shadow_array2[i]=CAImageManipulation::rotate(tmp_shadow2,(float)i/8*360.0,true);
			shadow_array3[i]=CAImageManipulation::rotate(tmp_shadow3,(float)i/8*360.0,true);
			shadow_array4[i]=CAImageManipulation::rotate(tmp_shadow4,(float)i/8*360.0,true);
			shadow_array5[i]=CAImageManipulation::rotate(tmp_shadow5,(float)i/8*360.0,true);
			shadow_array6[i]=CAImageManipulation::rotate(tmp_shadow6,(float)i/8*360.0,true);
			shadow_array7[i]=CAImageManipulation::rotate(tmp_shadow7,(float)i/8*360.0,true);
			shadow_array8[i]=CAImageManipulation::rotate(tmp_shadow8,(float)i/8*360.0,true);
			shadow_array9[i]=CAImageManipulation::rotate(tmp_shadow9,(float)i/8*360.0,true);
		}
		weapon0=CL_SoundBuffer::load("Sound/infweapon0",app->resources);
		move0=CL_SoundBuffer::load("Sound/infmove0",app->resources);
		cp1_x=x+30;
		cp1_y=y+30;
		Math::load_data(this);
		break;
	case 4:		//Crew
		is_turret=false;
		Math::load_data(this);
		number_of_soldiers=1+rand()%4;
		if (number_of_soldiers==1)
		{
			tmp_hull=CL_Surface::load("Graphics/Objects/rsoldier1",app->resources);
			tmp_shadow=CL_Surface::load("Graphics/Objects/rsoldier1_shadow",app->resources);
			for (i=0;i<8;i++)
			{
				hull_array[i]=CAImageManipulation::rotate(tmp_hull,(float)i/8*360.0,true);
				shadow_array[i]=CAImageManipulation::rotate(tmp_shadow,(float)i/8*360.0,true);
			}
			hull=hull_array[0];
			shadow=shadow_array[0];
		}
		else if (number_of_soldiers==2)
		{
			tmp_hull=CL_Surface::load("Graphics/Objects/rsoldier2",app->resources);
			tmp_shadow=CL_Surface::load("Graphics/Objects/rsoldier2_shadow",app->resources);
			for (i=0;i<8;i++)
			{
				hull_array[i]=CAImageManipulation::rotate(tmp_hull,(float)i/8*360.0,true);
				shadow_array[i]=CAImageManipulation::rotate(tmp_shadow,(float)i/8*360.0,true);
			}
			hull=hull_array[0];
			shadow=shadow_array[0];
		}
		else if (number_of_soldiers==3)
		{
			tmp_hull=CL_Surface::load("Graphics/Objects/rsoldier3",app->resources);
			tmp_shadow=CL_Surface::load("Graphics/Objects/rsoldier3_shadow",app->resources);
			for (i=0;i<8;i++)
			{
				hull_array[i]=CAImageManipulation::rotate(tmp_hull,(float)i/8*360.0,true);
				shadow_array[i]=CAImageManipulation::rotate(tmp_shadow,(float)i/8*360.0,true);
			}
			hull=hull_array[0];
			shadow=shadow_array[0];
		}
		else if (number_of_soldiers==4)
		{
			tmp_hull=CL_Surface::load("Graphics/Objects/rsoldier4",app->resources);
			tmp_shadow=CL_Surface::load("Graphics/Objects/rsoldier4_shadow",app->resources);
			for (i=0;i<8;i++)
			{
				hull_array[i]=CAImageManipulation::rotate(tmp_hull,(float)i/8*360.0,true);
				shadow_array[i]=CAImageManipulation::rotate(tmp_shadow,(float)i/8*360.0,true);
			}
			hull=hull_array[0];
			shadow=shadow_array[0];
		}
		move0=CL_SoundBuffer::load("Sound/infmove0",app->resources);
		cp1_x=x+30;
		cp1_y=y+30;
		break;
	}
}

void GameObject_Russian::show(int X,int Y,Kursk* app)
{
	if (is_visible)
	{
		shadow->put_screen(X+8,Y+8);
		hull->put_screen(X,Y);
		if (is_turret)
			turret->put_screen(X,Y);
	}
	if (is_burning)
	{
		//if (object_id==1)
			burning0->put_screen(X-10,Y-30,burning_frame);
		//else
		//	burning0->put_screen(X-4,Y-24,burning_frame);

		if (CL_System::get_time()>=burning_frame_delay+65)
		{
			burning_frame++;
			burning_frame_delay=CL_System::get_time();
		}

		if (burning_frame>31)
			burning_frame=15;
	}
}

void GameObject_Russian::update(Kursk* app)
{
	//int dest_x,dest_y;
	float angle,die_roll;
	bool finished=false;

	switch (app->game_state)
	{
	case manual_deployment:
		{for (std::list<GameObject*>::iterator it=app->objects.begin();it!=app->objects.end();it++)
			//we deploy only the russian units
			if (!(*it)->is_german)
			{
				switch ((*it)->object_index)
				{
				case 0:						//T-34
					(*it)->x=1536;
					(*it)->y=1104;
					(*it)->temp_x=(*it)->x;
					(*it)->temp_y=(*it)->y;
					(*it)->shadow=(*it)->shadow_array[4];
					(*it)->hull=(*it)->hull_array[4];
					(*it)->turret=(*it)->turret_array[4];
					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='#';
					(*it)->is_dugin=true;
					break;
				case 1:						//T-34
					(*it)->x=1824;
					(*it)->y=480;
					(*it)->temp_x=(*it)->x;
					(*it)->temp_y=(*it)->y;
					(*it)->shadow=(*it)->shadow_array[4];
					(*it)->hull=(*it)->hull_array[4];
					(*it)->turret=(*it)->turret_array[4];
					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='#';
					(*it)->is_dugin=true;
					break;
				case 2:						//T-34
					(*it)->x=1824;
					(*it)->y=576;
					(*it)->temp_x=(*it)->x;
					(*it)->temp_y=(*it)->y;
					(*it)->shadow=(*it)->shadow_array[4];
					(*it)->hull=(*it)->hull_array[4];
					(*it)->turret=(*it)->turret_array[4];
					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='#';
					(*it)->is_dugin=true;
					break;
				case 3:						//T-70
					(*it)->x=1392;
					(*it)->y=48;
					(*it)->temp_x=(*it)->x;
					(*it)->temp_y=(*it)->y;
					(*it)->shadow=(*it)->shadow_array[3];
					(*it)->hull=(*it)->hull_array[3];
					(*it)->turret=(*it)->turret_array[3];
					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='#';
					break;
				case 4:						//T-70
					(*it)->x=1488;
					(*it)->y=384;
					(*it)->temp_x=(*it)->x;
					(*it)->temp_y=(*it)->y;
					(*it)->shadow=(*it)->shadow_array[4];
					(*it)->hull=(*it)->hull_array[4];
					(*it)->turret=(*it)->turret_array[4];
					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='#';
					(*it)->is_dugin=true;
					break;
				case 5:						//120mm Mortar
					(*it)->x=1872;
					(*it)->y=1248;
					(*it)->temp_x=(*it)->x;
					(*it)->temp_y=(*it)->y;
					(*it)->shadow=(*it)->shadow_array[4];
					(*it)->hull=(*it)->hull_array[4];
					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='#';
					break;
				case 6:						//Infantry Sqd
					(*it)->x=1104;
					(*it)->y=480;
					(*it)->temp_x=(*it)->x;
					(*it)->temp_y=(*it)->y;
					(*it)->shadow=(*it)->shadow_array[4];
					(*it)->hull=(*it)->hull_array[4];
					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='#';
					break;
				case 7:						//Infantry Sqd
					(*it)->x=864;
					(*it)->y=720;
					(*it)->temp_x=(*it)->x;
					(*it)->temp_y=(*it)->y;
					(*it)->shadow=(*it)->shadow_array[4];
					(*it)->hull=(*it)->hull_array[4];
					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='#';
					break;
				case 8:						//Infantry Sqd
					(*it)->x=1008;
					(*it)->y=960;
					(*it)->temp_x=(*it)->x;
					(*it)->temp_y=(*it)->y;
					(*it)->shadow=(*it)->shadow_array[4];
					(*it)->hull=(*it)->hull_array[4];
					app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+24)/TILE_WIDTH]='#';
					break;
				}
			}
		}

		break;

	case r_fire:

		//reset the interface messaging
		app->scenario_interface->message1_target_type="";
		app->scenario_interface->message2_target_distance="";
		app->scenario_interface->message3_hit_chance="";

		{for (std::list<GameObject*>::iterator it=app->objects.begin();it!=app->objects.end();it++)

			if ((*it)->is_firing && !(*it)->is_german)
			{
				if ((*it)->shot_maingun+(*it)->shot_coax > 0)
				{
					(*it)->shell=new projectile(app,(*it),(*it));
					(*it)->blt_priority=1;

					//sound effects
					if ((*it)->object_id==0 || (*it)->object_id==1 || (*it)->object_id==2)
						if ((*it)->shot_maingun > 0)
							(*it)->weapon0->play();
						else
							(*it)->cmgweapon0->play();
					else 
						(*it)->weapon0->play();

					app->scenario_interface->message4_hit_results="";
					app->scenario_interface->message5_hit_results="";
					app->scenario_interface->message6_hit_results="";
						
					//decrease the number of available shots
					if ((*it)->object_id != 3)
						if ((*it)->shot_maingun > 0)
							(*it)->shot_maingun--;
						else
							(*it)->shot_coax--;
					else
						(*it)->shot_coax--;


                     //turn the turret, if any 
                     angle=atan2((*it)->shell->dest_y-(*it)->shell->start_y,
	                     (*it)->shell->dest_x-(*it)->shell->start_x);

					 if ((*it)->is_turret)
						(*it)->turret=CAImageManipulation::rotate((*it)->tmp_turret,(float)RADtoDEG(angle),true);
					 else if ((*it)->object_id==3)
					 {

						switch ((*it)->number_of_soldiers)
						{
						case 1:
							(*it)->shadow=CAImageManipulation::rotate((*it)->tmp_shadow1,(float)RADtoDEG(angle),true);
							(*it)->hull=CAImageManipulation::rotate((*it)->tmp_hull1,(float)RADtoDEG(angle),true);
							break;
						case 2:
							(*it)->shadow=CAImageManipulation::rotate((*it)->tmp_shadow2,(float)RADtoDEG(angle),true);
							(*it)->hull=CAImageManipulation::rotate((*it)->tmp_hull2,(float)RADtoDEG(angle),true);
							break;
						case 3:
							(*it)->shadow=CAImageManipulation::rotate((*it)->tmp_shadow3,(float)RADtoDEG(angle),true);
							(*it)->hull=CAImageManipulation::rotate((*it)->tmp_hull3,(float)RADtoDEG(angle),true);
							break;
						case 4:
							(*it)->shadow=CAImageManipulation::rotate((*it)->tmp_shadow4,(float)RADtoDEG(angle),true);
							(*it)->hull=CAImageManipulation::rotate((*it)->tmp_hull4,(float)RADtoDEG(angle),true);
							break;
						case 5:
							(*it)->shadow=CAImageManipulation::rotate((*it)->tmp_shadow5,(float)RADtoDEG(angle),true);
							(*it)->hull=CAImageManipulation::rotate((*it)->tmp_hull5,(float)RADtoDEG(angle),true);
							break;
						case 6:
							(*it)->shadow=CAImageManipulation::rotate((*it)->tmp_shadow6,(float)RADtoDEG(angle),true);
							(*it)->hull=CAImageManipulation::rotate((*it)->tmp_hull6,(float)RADtoDEG(angle),true);
							break;
						case 7:
							(*it)->shadow=CAImageManipulation::rotate((*it)->tmp_shadow7,(float)RADtoDEG(angle),true);
							(*it)->hull=CAImageManipulation::rotate((*it)->tmp_hull7,(float)RADtoDEG(angle),true);
							break;
						case 8:
							(*it)->shadow=CAImageManipulation::rotate((*it)->tmp_shadow8,(float)RADtoDEG(angle),true);
							(*it)->hull=CAImageManipulation::rotate((*it)->tmp_hull8,(float)RADtoDEG(angle),true);
							break;
						case 9:
							(*it)->shadow=CAImageManipulation::rotate((*it)->tmp_shadow9,(float)RADtoDEG(angle),true);
							(*it)->hull=CAImageManipulation::rotate((*it)->tmp_hull9,(float)RADtoDEG(angle),true);
							break;
						case 10:
							(*it)->shadow=CAImageManipulation::rotate((*it)->tmp_shadow,(float)RADtoDEG(angle),true);
							(*it)->hull=CAImageManipulation::rotate((*it)->tmp_hull,(float)RADtoDEG(angle),true);
							break;
						}

					 }

                     app->game_state=r_do_fire;

                     break;//<***************************I changed this!!!
				}
				else if ((*it)->object_id==2)
				{
					(*it)->shell=new projectile(app,(*it),(*it));
					(*it)->is_firing=true;
					(*it)->blt_priority=1;
				
					(*it)->explosion0->play();
							
					app->crater_visible=false;

					app->scenario_interface->message4_hit_results="";
					app->scenario_interface->message5_hit_results="";
					app->scenario_interface->message6_hit_results="";

					//decrease the number of available shots
					if ((*it)->shot_maingun > 0)
						(*it)->shot_maingun--;
					else
						(*it)->shot_coax--;

					app->game_state=r_do_fire;

					break;
					
				}
				
				//break;
			}
		}

		break;

	case r_do_fire:

		{for (std::list<GameObject*>::iterator it=app->objects.begin();it!=app->objects.end();it++)

			if ((*it)->is_firing && !(*it)->is_german)
			{
				if (!(*it)->shell->is_dead)
				{
					(*it)->shell->update(app);
					(*it)->shell->show(app);
				}
				else
				{
					if ((*it)->shell->frame_counter<12)
					{
						(*it)->shell->explosion->put_screen((*it)->shell->dest_x-TILE_WIDTH/2-app->map_x,
                            (*it)->shell->dest_y-TILE_HEIGHT/2-app->map_y,(*it)->shell->frame_counter);

						if (CL_System::get_time()>=(*it)->shell->projectile_frame_delay+65)
						{
							(*it)->shell->frame_counter++;
							(*it)->shell->projectile_frame_delay=CL_System::get_time();
						}						
					}
					else
					{
						(*it)->is_firing=false;
						(*it)->blt_priority=0;
						app->crater_visible=true;

						if (!app->russian_turn)
							app->game_state=g_movement;
						else
							app->game_state=r_fire_phase;

						break;
					}
				}
			}
		}

		break;

	case r_fire_phase:

		Enemy::enemy_ai(app);

		if (CL_System::get_time()>app->turn_delay+6000)
			app->game_state=r_recovery;

		break;

	case r_recovery:

		srand((unsigned)time(NULL));
		
		{for (std::list<GameObject*>::iterator it=app->objects.begin();it!=app->objects.end();it++)

			if (!(*it)->is_german && !(*it)->is_dead)
			{
				//rally suppressed units
				if ((*it)->suppressed>0)
				{
					for (int i=1;i<=(*it)->suppressed;i++)
					{
						die_roll=rand()%12;

						if (die_roll<=(*it)->morale)
							(*it)->suppressed--;
					}
					//if it's still suppressed after rallying
					if ((*it)->suppressed>0)
					{
						(*it)->shot_maingun=0;
						(*it)->shot_coax=0;
						(*it)->movement_point=0;
						(*it)->object_status=3;
					}
				}
				//rally pinned and routed units
				else if ((*it)->object_status==2 || (*it)->object_status==3 ||
					(*it)->object_status==8)
				{
					die_roll=rand()%12;

					if (die_roll<=(*it)->morale)
					{
						switch ((*it)->object_id)
						{
							case 0:			//T-34
								(*it)->shot_maingun=3;
								(*it)->shot_coax=3;
								(*it)->movement_point=288;
								(*it)->object_status=0;
								(*it)->combat_strength=-10;
								break;
							case 1:			//T-70
								(*it)->shot_maingun=4;
								(*it)->shot_coax=3;
								(*it)->movement_point=336;
								(*it)->object_status=0;
								(*it)->combat_strength=-6;
								break;
							case 2:			//120mm Mortar
								(*it)->shot_maingun=4;
								(*it)->shot_coax=0;
								(*it)->movement_point=0;
								(*it)->object_status=0;
								(*it)->combat_strength=0;//-4
								break;
							case 3:			//Infantry
								(*it)->shot_maingun=0;
								(*it)->shot_coax=3;
								(*it)->movement_point=240;
								(*it)->object_status=0;
								(*it)->combat_strength=-5;
								break;
							case 4:			//crew
								(*it)->shot_maingun=0;
								(*it)->shot_coax=0;
								(*it)->movement_point=144;
								(*it)->object_status=0;
								(*it)->combat_strength=-1;
								break;
						}
					}
					else
					{
						(*it)->shot_maingun=0;
						(*it)->shot_coax=0;
						(*it)->movement_point=0;
						(*it)->object_status=3;
					}
				}
				//repair damaged units
				else if ((*it)->object_status==1)		//gun malfunction
				{
					die_roll=rand()%20;
					//unit is repaired
					if (die_roll>=1 && die_roll<=4)
					{
						switch ((*it)->object_id)
						{
							case 0:			//T-34
								(*it)->shot_maingun=3;
								(*it)->shot_coax=3;
								(*it)->movement_point=288;
								(*it)->object_status=0;
								(*it)->combat_strength=-10;
								break;
							case 1:			//T-70
								(*it)->shot_maingun=4;
								(*it)->shot_coax=3;
								(*it)->movement_point=336;
								(*it)->object_status=0;
								(*it)->combat_strength=-6;
								break;
							case 2:			//120mm Mortar
								(*it)->shot_maingun=4;
								(*it)->shot_coax=0;
								(*it)->movement_point=0;
								(*it)->object_status=0;
								(*it)->combat_strength=0;//-4
								break;							
						}
					}
					else
					{
						(*it)->shot_maingun=0;
						(*it)->shot_coax=0;
						(*it)->movement_point=0;
						(*it)->object_status=3;
					}
				}
				else if ((*it)->object_status==7)		//immobilized
				{
					(*it)->shot_maingun=1;
					(*it)->shot_coax=1;
					(*it)->movement_point=0;
					(*it)->object_status=7;
					(*it)->combat_strength=-3;

					//bail-out check
					die_roll=rand()%12;
					//if fails the crew is bailing-out
					if (die_roll>(*it)->morale)
					{
						(*it)->is_dead=true;
						app->scenario_interface->message6_hit_results=CL_String("Crew is bailing out!");
						(*it)->object_status=4;			//abandoned
						(*it)->movement_point=0;
						(*it)->move0->stop();
						(*it)->combat_strength=0;
						app->number_of_russians++;
						app->objects.push_back(new GameObject_Russian((*it)->x+60,(*it)->y,4,app->number_of_russians,app));
						app->scenario_map->drive_map[(int)((*it)->y+24)/TILE_HEIGHT][(int)((*it)->x+84)/TILE_WIDTH]='#';
					}
				}
				//these units are OK
				else if ((*it)->object_status==0)
				{
					switch ((*it)->object_id)
					{
						case 0:			//T-34
							(*it)->shot_maingun=1+rand()%2;
							(*it)->shot_coax=rand()%3;
							(*it)->movement_point=288;
							(*it)->object_status=0;
							(*it)->combat_strength=-10;
							break;
						case 1:			//T-70
							(*it)->shot_maingun=1+rand()%3;
							(*it)->shot_coax=rand()%3;
							(*it)->movement_point=336;
							(*it)->object_status=0;
							(*it)->combat_strength=-6;
							break;
						case 2:			//120mm Mortar
							(*it)->shot_maingun=1+rand()%3;
							(*it)->shot_coax=0;
							(*it)->movement_point=0;
							(*it)->object_status=0;
							(*it)->combat_strength=0;//-4
							break;
						case 3:			//Infantry
							(*it)->shot_maingun=0;
							(*it)->shot_coax=1+rand()%2;
							(*it)->movement_point=240;
							(*it)->object_status=0;
							(*it)->combat_strength=-5;
							break;
						case 4:			//crew
							(*it)->shot_maingun=0;
							(*it)->shot_coax=0;
							(*it)->movement_point=144;
							(*it)->object_status=0;
							(*it)->combat_strength=-1;
							break;
					}
				}
			}
		}
		if (app->r_is_artillery)
			app->game_state=r_artillery;
		else
		{
			Enemy::enemy_movement(app);
			app->turn_delay=CL_System::get_time();
			app->game_state=r_movement;
		}

		break;

	case r_movement:

		move(app);

		break;
	}
}

void GameObject_Russian::move(Kursk* app)
{
	int dest_x,dest_y;

	if (do_astar && !is_german)
	{
		if	(unit_path->NewPath(x,y,target_x,target_y))
		{
			left=right=top=bottom=false;

			app->scenario_map->drive_map[(int)((temp_y+24)/TILE_HEIGHT)][(int)((temp_x+24)/TILE_WIDTH)]='|';

			unit_path->PathNextNode();
			dest_x=unit_path->NodeGetX();
			dest_y=unit_path->NodeGetY();

			if (!unit_path->ReachedGoal() && dest_x==x && dest_y==y)
			{
				unit_path->PathNextNode();
				dest_x=unit_path->NodeGetX();
				dest_y=unit_path->NodeGetY();
			}
			///////////////////////////////////////////////////////
			if (dest_x > x)
			{
				x+=speed;
				right=true;

				if (tile_hit(app))
				{
					x-=speed;
					left=true;
				}
				movement_point-=speed;
			}
			if (dest_y > y)
			{
				y+=speed;
				bottom=true;

				if (tile_hit(app))
				{
					y-=speed;
					top=true;
				}
				movement_point-=speed;
			}
			if (dest_x < x)
			{
				x-=speed;
				left=true;

				if (tile_hit(app))
				{
					x+=speed;
					right=true;
				}
				movement_point-=speed;
			}
			if (dest_y < y)
			{
				y-=speed;
				top=true;

				if (tile_hit(app))
				{
					y+=speed;
					bottom=true;
				}
				movement_point-=speed;
			}
			//calculate the direction of movement
			if (is_turret)
			{
				shadow=shadow_array[calculate_dir()];
				hull=hull_array[calculate_dir()];
				turret=turret_array[calculate_dir()];
			}
			else if (object_id==3)
			{
				switch (number_of_soldiers)
				{
				case 1:
					shadow=shadow_array1[calculate_dir()];
					hull=hull_array1[calculate_dir()];
					break;
				case 2:
					shadow=shadow_array2[calculate_dir()];
					hull=hull_array2[calculate_dir()];
					break;
				case 3:
					shadow=shadow_array3[calculate_dir()];
					hull=hull_array3[calculate_dir()];
					break;
				case 4:
					shadow=shadow_array4[calculate_dir()];
					hull=hull_array4[calculate_dir()];
					break;
				case 5:
					shadow=shadow_array5[calculate_dir()];
					hull=hull_array5[calculate_dir()];
					break;
				case 6:
					shadow=shadow_array6[calculate_dir()];
					hull=hull_array6[calculate_dir()];
					break;
				case 7:
					shadow=shadow_array7[calculate_dir()];
					hull=hull_array7[calculate_dir()];
					break;
				case 8:
					shadow=shadow_array8[calculate_dir()];
					hull=hull_array8[calculate_dir()];
					break;
				case 9:
					shadow=shadow_array9[calculate_dir()];
					hull=hull_array9[calculate_dir()];
					break;
				case 10:
					shadow=shadow_array[calculate_dir()];
					hull=hull_array[calculate_dir()];
					break;
				}
			}

			//check if the unit is on a victory location
			if (get_cp1_x()>=1728 && get_cp1_x()<=1776 && 
				get_cp1_y()>=192 && get_cp1_y()<=240)
			{
				app->scenario_map->victory_flag1=0;
			}
			else if (get_cp1_x()>=1488 && get_cp1_x()<=1536 && 
				get_cp1_y()>=624 && get_cp1_y()<=672)
			{
				app->scenario_map->victory_flag2=0;
			}
			else if (get_cp1_x()>=1824 && get_cp1_x()<=1872 && 
				get_cp1_y()>=1104 && get_cp1_y()<=1152)
			{
				app->scenario_map->victory_flag3=0;
			}
			if (movement_point<=0)
			{
				//run out of movement points
				do_astar=false;
				move0->stop();
				delete unit_path;

				temp_x=x;
				temp_y=y;
				app->scenario_map->drive_map[(int)((y+24)/TILE_HEIGHT)][(int)((x+24)/TILE_WIDTH)]='#';				
			}
		}
		else
		{
			//goal is reached!
			do_astar=false;
			delete unit_path;
			move0->stop();

			temp_x=x;
			temp_y=y;
			app->scenario_map->drive_map[(int)((y+24)/TILE_HEIGHT)][(int)((x+24)/TILE_WIDTH)]='#';				
		}
	}
}

int GameObject_Russian::hit_check(GameObject* source,Kursk* app)
{

	return (0);
}

GameObject_Russian::enemy_seen(Kursk* app)
{
	enemy_number=0;

	{for (std::list<GameObject*>::iterator it=app->objects.begin();it!=app->objects.end();it++)
		//don,t check itself	
		if ((*it)->is_german)
		{
			//reset the visible_by variable, which stores the object_index
			(*it)->visible_by=255;

			//check for visibility distance
			if (Math::distance(x,y,(*it)->x,(*it)->y) <= target_visibility)
			{
				//next check for obstacles in LOS and don't count wrecked units
				if(!Math::obstacle_height(app,x,y,(*it)->x,(*it)->y) &&
					!(*it)->is_dead)
				{
					//store the german unit id's the russian unit sees
					enemy_type[enemy_number]=(*it)->object_id;
					enemy_index[enemy_number]=(*it)->object_index;
					enemy_x[enemy_number]=(*it)->x;
					enemy_y[enemy_number]=(*it)->y;
					enemy_hull_frame[enemy_number]=(*it)->hull_frame;
					enemy_morale[enemy_number]=(*it)->morale;
					if ((*it)->is_turret)
						enemy_turret_frame[enemy_number]=(*it)->turret_frame;
					enemy_number++;
				}
			}
		}
	}

	return (enemy_number);
}
