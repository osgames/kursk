/***************************************************************************
                          Button.cpp  -  description
                             -------------------
    copyright            : (C) 2000 by Kalman Andrasi
    email                : andrasik@un.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include "Main.h"
#include "Button.h"

/** New button constructor.
    \param app   the application
    \param _x     
    \param _y    x,y screen coordinates of the button
    \param type  button type (artillery,move,fire,endturn,rally,quit,continue,
				 begin,cancel)
*/
Button::Button(Kursk* app,int _x,int _y,int type)
{
	x=_x;
	y=_y;

	switch (type)
	{
	case artillery:	//artillery button
		button_surface=CL_Surface::load("Graphics/Interface/arty_button",app->resources);
		button_surface_down=CL_Surface::load("Graphics/Interface/arty_button_down",app->resources);
		button_surface_notavail=CL_Surface::load("Graphics/Interface/arty_button_notavail",app->resources);
		break;

	case move:		//move button
		button_surface=CL_Surface::load("Graphics/Interface/move_button",app->resources);
		button_surface_down=CL_Surface::load("Graphics/Interface/move_button_down",app->resources);
		button_surface_notavail=CL_Surface::load("Graphics/Interface/move_button_notavail",app->resources);
		break;

	case fire:		//fire button
		button_surface=CL_Surface::load("Graphics/Interface/fire_button",app->resources);
		button_surface_down=CL_Surface::load("Graphics/Interface/fire_button_down",app->resources);
		button_surface_notavail=CL_Surface::load("Graphics/Interface/fire_button_notavail",app->resources);
		break;
	case endturn:	//endturn button
		button_surface=CL_Surface::load("Graphics/Interface/endturn_button",app->resources);
		button_surface_down=CL_Surface::load("Graphics/Interface/endturn_button_down",app->resources);
		button_surface_notavail=CL_Surface::load("Graphics/Interface/endturn_button_notavail",app->resources);
		break;
	case rally:		//rally button
		button_surface=CL_Surface::load("Graphics/Interface/rally_button",app->resources);
		button_surface_down=CL_Surface::load("Graphics/Interface/rally_button_down",app->resources);
		button_surface_notavail=CL_Surface::load("Graphics/Interface/rally_button_notavail",app->resources);
		break;
	case quit:		//quit button
		button_surface=CL_Surface::load("Graphics/Interface/quit_button",app->resources);
		button_surface_down=CL_Surface::load("Graphics/Interface/quit_button_down",app->resources);
		button_surface_notavail=CL_Surface::load("Graphics/Interface/quit_button_notavail",app->resources);
		break;
	case cont:		//continue button
		button_surface=CL_Surface::load("Graphics/Interface/continue_button",app->resources);
		button_surface_down=CL_Surface::load("Graphics/Interface/continue_button_down",app->resources);
		break;
	case begin:		//begin button
		button_surface=CL_Surface::load("Graphics/Interface/begin_button",app->resources);
		button_surface_down=CL_Surface::load("Graphics/Interface/begin_button_down",app->resources);
		break;
	case cancel:	//cancel button
		button_surface=CL_Surface::load("Graphics/Interface/cancel_button",app->resources);
		button_surface_down=CL_Surface::load("Graphics/Interface/cancel_button_down",app->resources);
		break;
	case waypoint:
		button_surface=CL_Surface::load("Graphics/Interface/waypoint_button",app->resources);
		button_surface_down=CL_Surface::load("Graphics/Interface/waypoint_button_down",app->resources);
		button_surface_notavail=CL_Surface::load("Graphics/Interface/waypoint_button_notavail",app->resources);
		break;
	case group:
		button_surface=CL_Surface::load("Graphics/Interface/group_button",app->resources);
		button_surface_down=CL_Surface::load("Graphics/Interface/group_button_down",app->resources);
		button_surface_notavail=CL_Surface::load("Graphics/Interface/group_button_notavail",app->resources);
		break;
	}
}

/** Draw a button
	\param state	1 - available,
					0 - not available
*/
void Button::draw(int state)
{
	if (state==0)		//not available
		button_surface_notavail->put_screen(x,y);
	else
		button_surface->put_screen(x,y);
}

/** if the button is pressed, returns true	
*/
bool Button::is_pressed()
{
	if ((CL_Mouse::get_x()>=x && CL_Mouse::get_x()<=x+button_surface->get_width()) &&
		(CL_Mouse::get_y()>=y && CL_Mouse::get_y()<=y+button_surface->get_height()) &&
		CL_Mouse::left_pressed())
	{
		button_surface_down->put_screen(x,y);
		CL_Display::flip_display();
						
		CL_System::sleep(50);
		return true;
	}

	return false;
}
